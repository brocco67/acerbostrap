<?php get_header(); ?>
    <div id="content" class="cf col-md-8 bd-right">

        <h1 class="title compensate-bs">
            <?php
            $args = '';
            $date = date('Y-m-d');
            if (is_author()) {
	            the_archive_title();
            } elseif (is_day()) {
                printf('Archivi giornalieri', get_the_date());
            } elseif (is_month()) {
                echo 'Archivi mensili ';
                echo ucfirst(get_the_date('F Y'));
            } elseif (is_year()) {
                echo 'Archivi annuali ';
                echo ucfirst(get_the_date('Y'));
            } elseif (is_post_type_archive('contributo')) {
	            echo 'Succede all\'Acerbo: le attività dei docenti';
            } elseif (is_post_type_archive('circolare')) {
                echo 'Tutte le circolari';
            } elseif (is_post_type_archive('avviso')) {
                echo 'Tutti gli avvisi';
            } elseif (is_post_type_archive('appuntamento')) {
                echo 'Tutti gli appuntamenti';
            } elseif (is_post_type_archive('foto_e_video')) {
	            echo 'Tutte le foto e i video dell\'Acerbo';
            } elseif (is_category()) {
                echo ucfirst(get_queried_object()->cat_name);
            } elseif (is_tag()) {
                echo ucfirst(get_queried_object()->name);
                $args = array('meta_query' => array(
                    array(
                        'key' => 'sticked',
                        'value' => $date,
                        'compare' => '>',
                        'type' => 'date'
                    )
                ),
                    'tag' => get_queried_object()->slug);
            } elseif (is_tax()) {
                if (get_queried_object()->parent != 0) {
                    echo ucfirst(get_term(get_queried_object()->parent, get_queried_object()->taxonomy)->name) . ': ';
                }
                echo ucfirst(get_queried_object()->name);
                $args = array(
                    'meta_query' => array(
                        array(
                            'key' => 'sticked',
                            'value' => $date,
                            'compare' => '>',
                            'type' => 'date'
                        )
                    ),

                    'tax_query' =>
                        array(
                            array('taxonomy' => get_queried_object()->taxonomy,
                                'field' => 'slug',
                                'terms' => get_queried_object()->slug)
                        )
                );
            } else {
                echo 'Archivi';
            }
            ?>
        </h1>

        <div class="archive-bodycopy">

            <?php
            //Eseguo la query per gli elementi sticked
            $the_sticked = new WP_Query($args);
            if ($the_sticked->have_posts()) : ?>

                <h2>In evidenza</h2>

                <?php
                while ($the_sticked->have_posts()) : $the_sticked->the_post();
                    get_template_part('acerbo', 'loop');
                endwhile;
                echo '<hr class="styled"/>';
            endif;
            wp_reset_postdata();

            //Eseguo la main query
            if (have_posts()) : while (have_posts()): the_post();
                get_template_part('acerbo', 'loop');
            endwhile;
            else :
                ?>
                <div class="post not-found">
                    <h2>Non abbiamo trovato nulla</h2>

                    <div class="post-bodycopy">
                        <p>Ci dispiace ma questa pagina non contiene articoli. Può provare ad usare il nostro motore di
                            ricerca per cercare il contenuto che le serve.</p>
                        <?php get_search_form(); ?>
                    </div>
                </div>
            <?php endif; ?>
        </div>
        <?php if (function_exists('wp_pagenavi')) : ?>
            <?php wp_pagenavi(); ?>
        <?php else : ?>
            <div class="alignleft"><?php next_posts_link('Contenuti più vecchi'); ?></div>
            <div class="alignright"><?php previous_posts_link('Contenuti più nuovi'); ?></div>
        <?php endif; ?>
    </div>
    <div id="widgetarea-one" class="col-md-4 bd-left-minus">
        <h1 class="title compensate-bs" style="margin-bottom: 25px">Sezioni</h1>
        <?php get_template_part('sidebar', 'archive'); ?>
    </div>

<?php get_footer(); ?>