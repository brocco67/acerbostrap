<?php
get_header();
$permalink = get_permalink();
?>

<div id="content" class="col-md-8 bd-right">
    <?php
    // Start the Loop.
    while (have_posts()) : the_post();
        ?>
        <div id="post-<?php the_ID(); ?>-evento" <?php post_class('acerbo-entry'); ?>>

            <?php
            if (has_post_thumbnail()) {
                echo '<div class="post-thumb compensate-bs"><div class="thumb-inner">';
                the_post_thumbnail('showed');
                echo '</div></div>';
            }
            ?>
            <div class="page-header">
                <h1 class="title compensate-bs">
                    <?php the_title(); ?>
                </h1>
            </div>

            <div class="post-bodycopy">

                <?php
                echo '<p>Ora e luogo: ';
                $start = get_post_meta($post->ID, 'evento_unixtime_start', true) ? get_post_meta($post->ID, 'evento_unixtime_start', true) : 0;
                $end = get_post_meta($post->ID, 'evento_unixtime_end', true) ? get_post_meta($post->ID, 'evento_unixtime_end', true) : 0;
                $luogo = get_post_meta($post->ID, 'evento_luogo', true) ? get_post_meta($post->ID, 'evento_luogo', true) : 0;
                if (( $end - $start ) > 86400) {
                    if (( gmdate("m", $start) != gmdate("m", $end))) {
                        $startdate = date_i18n('j F', $start);
                        $enddate = date_i18n('j F', $end);
                        echo '<i class="fa fa-clock-o fa-fw"></i><span class="sr-only">Ora dell\'evento</span> ' . $startdate . ' - ' . $enddate;
                    } else {
                        $startdate = date_i18n('j', $start);
                        $enddate = date_i18n('j F', $end);
                        echo '<i class="fa fa-clock-o fa-fw"></i><span class="sr-only">Ora dell\'evento</span> ' . $startdate . ' - ' . $enddate;
                    }
                } else {
                    if (( gmdate("Hi", $end) == '0000')) {
                        $startdate = date_i18n('j F G:i', $start);
                        echo '<i class="fa fa-clock-o fa-fw"></i><span class="sr-only">Ora dell\'evento</span> ' . $startdate;
                    } else {
                        $startdate = date_i18n('j F G:i', $start);
                        $enddate = date_i18n('G:i', $end);
                        echo '<i class="fa fa-clock-o fa-fw"></i><span class="sr-only">Ora dell\'evento</span> ' . $startdate . ' - ' . $enddate;
                    }
                }

                if (!empty($luogo)) {
                    echo '<i class="fa fa-map-marker fa-fw"></i><span class="sr-only">Luogo dell\'evento</span>' . $luogo;
                }
                echo '</p>';
                ?>
                
                

                <?php the_content(); ?>
                <?php
                wp_link_pages(array(
                    'before' => __('<p class="post-pagination">Pages:', 'montezuma'),
                    'after' => '</p>'
                ));
                ?>
            </div>
            <p style="text-align: center;margin: 30px 0;"><a class="btn btn-default" href="/appuntamenti/" role="button">Visualizza tutti gli appuntamenti</a></p>
            <!--div class="alert alert-info" role="alert">Se vuoi ricevere gli aggiornamenti di questo sito via posta elettronica <a href="/newsletter" class="alert-link">iscriviti alla nostra newsletter</a>.</div-->
            <hr class="styled" />
            <div class="post-share">
                <span><i class="fa fa-share-square fa-fw"></i> Condividi:&nbsp;&nbsp;
                    <a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo $permalink; ?>" title="Condividi su Facebook" class="wn-pop"><i class="fa fa-facebook fa-lg icon-facebook"></i></a>&nbsp;&nbsp;&nbsp;&nbsp;
                    <a href="https://twitter.com/share?url=<?php echo $permalink; ?>" title="Condividi su Twitter" class="wn-pop"><i class="fa fa-twitter fa-lg icon-twitter"></i></a>&nbsp;&nbsp;&nbsp;&nbsp;
                </span>
            </div>
            <div class="post-footer">
                <?php
                echo '<span><i class="fa fa-clock-o fa-fw"></i>&nbsp;Pagina pubblicata il ' . get_the_time('j F Y') . '</span>';
                echo get_the_term_list($post->ID, 'category', '<span><i class="fa fa-bookmark fa-fw"></i>&nbsp;Categorie:', '&middot;', '</span>');
                echo get_the_term_list($post->ID, 'tema', '<span><i class="fa fa-bookmark fa-fw"></i>&nbsp;Temi:', '&middot;', '</span>');
                echo get_the_term_list($post->ID, 'post_tag', '<span><i class="fa fa-tags fa-fw"></i>&nbsp;Tag:', '&middot;', '</span>');
                the_image_credits('', '<span><i class="fa fa-picture-o fa-fw"></i>&nbsp;Immagine: ', '</span>');
                ?>
            </div>

            <?php
            if (comments_open() || get_comments_number()) {
                echo '<hr class="styled" />';
                comments_template();
            }
            ?>
            <hr class="styled" />
            <nav class="singlenav">
                <div class="older"><?php previous_post_link(); ?></div>
                <div class="newer"><?php next_post_link(); ?></div>
            </nav>

            <?php
        endwhile;
        ?>

    </div>
</div>

<div id="widgetarea-one" class="col-md-4 bd-left-minus">
    <h1 class="title compensate-bs" style="margin-bottom: 25px">Sezioni</h1>
    <?php get_template_part('sidebar', 'archive'); ?>     
</div>
<?php get_footer(); ?>