/*
 * Funzioni per gestione del menu
 */

var ie8 = navigator.userAgent.match(/Trident\/4\./) ? true : false;

function menuIntentOpen() {
    console.log('A intent open ' + menuOpen);
    var el = $(this);
    if (menuOpen === 0 && el.hasClass('menu-item-has-children')) {
        menuOpen = 1;
        console.log('A alzo child ' + menuOpen);
        $('.custom-sub').hide('fast');
        $('.custom-sub', el).fadeIn('fast');
    } else {
        if (el.hasClass('menu-item-has-children') && el.find('.sub-menu').length != 0) {
            console.log('A chiudo e apro sub 02 ' + menuOpen);
            $('.custom-sub').hide();
            $('.custom-sub', el).show();
            return false;
        } else {
            if (menuOpen === 1 && !el.parent().hasClass('sub-menu')) {
                menuOpen = 0;
                console.log('A abbasso child e nascondo sub ' + menuOpen);
                $('.custom-sub').fadeOut('fast');
            }
        }
    }
}

function menuIntentClose() {
    console.log('B intent open ' + menuOpen);
    if (menuOpen === 1) {
        menuOpen = 0;
        console.log('B abbasso child e nascondo sub ' + menuOpen);
        $('.custom-sub').fadeOut('fast');

    }
}

/*
 * Gestione delle label delle novità
 */

function runLabel(position) {
    var lastVisit = (docCookies.getItem('newtime')) ? Math.floor(docCookies.getItem('newtime') / 1000) : Math.floor((Date.now() / 1000) - 86400);
    var notification = true;
    if (docCookies.getItem('thisVisit')) {
        var itemVisit = docCookies.getItem('thisVisit');
        notification = false;
    } else {
        var itemVisit = lastVisit;
        docCookies.setItem('thisVisit', lastVisit, null, '/');
    }
    console.log('ultima visita :' + lastVisit);
    console.log('questa visita :' + itemVisit);
    var newnews = 0;
    var newviewed = [];
    $('#aggiornamenti').find('h2').each(function () {
        var el = $(this);
        if (el.find('span.label-new').length === 0) {
            if ((el.data('updated') > itemVisit)) {
                $('<span class="label-new"><i class="fa fa-check-circle"></i></span>').hide().prependTo(el).fadeIn();
                if ($.inArray(crc32(el.text()), newviewed) == -1) {
                    newnews++;
                }
                newviewed.push(crc32(el.text()));
            }
        }
    });
    $('#inprimopiano').find('h2').each(function () {
        var el = $(this);
        if (el.find('span.label-primopiano').length === 0) {
            if ((el.data('published') > itemVisit)) {
                $('<span class="label label-primary label-primopiano">Novità</span>').hide().appendTo(el).fadeIn();
            } else if ((el.data('updated') > itemVisit)) {
                $('<span class="label label-primary label-primopiano">Aggiornato</span>').hide().appendTo(el).fadeIn();
            }
        }
    });
    if (jQuery('body').hasClass('home') == true) {
        console.log('ultimo newnews ' + newnews);
        if (newnews != 0) {
            if (notification == true) {
                $('<span id="label-new" class="label label-success">' + newnews + '</span>').hide().appendTo('.menu-novita a').fadeIn();
                $('.fs-navigation-handle').addClass('notification').attr('data-content', newnews);
            }
            if (newnews == 1) {
                $('.menu-novita a').attr('title', 'Un nuovo contenuto dall\'ultima tua visita');
            } else {
                $('.menu-novita a').attr('title', newnews + ' nuovi contenuti dall\'ultima tua visita');
            }
        }
    }
}

/*
 * Gestione del pulsante download
 */

function runDownloadButton() {
    jQuery(document).ready(function ($) {
        console.log('eseguo download button');
        if ((("download" in document.createElement("a")) !== true)) {
            console.log('mostro phpdownload');
            $('.phpdownload').show();
        } else {
            console.log('mostro htmldownload');
            $('.htmldownload').show();
        }
    });
}

/*
 * Tooltip
 */

function runTooltip(type) {
    if (type == 'mobile') {
        if ((docCookies.getItem('tooltip') != 'menu')) {
            console.log('eseguo tooltip mobile');
            setTimeout(function () {
                $('.mobile_handle').tooltip('show');
            }, 2000);
            setTimeout(function () {
                $('.mobile_handle').tooltip('hide');
            }, 10000);
            $('.mobile_handle').one('touchstart click', function () {
                $('.mobile_handle').tooltip('destroy');
                docCookies.setItem('tooltip', 'menu', 2678400, '/');
            });
        }
    } else if (type == 'desktop') {
        if ($(".doc__button").length > 0) {
            console.log('eseguo tooltip desktop');
            $(function () {
                $('[data-toggle="tooltip"]').tooltip();
            });
        }
    }
}

/*
 * Fullvideo
 */

$(document).ready(function () {
    /* Silenzio il video */
    $("#fullvideo-video-mute").click(function () {
        $('#fullvideo').prop('muted', !$('#fullvideo').prop('muted'));
    });

    /* Metto in pausa il video */
    $("#fullvideo-video-pause").click(function () {
        if ($('#fullvideo').prop('paused')) {
            $('#fullvideo')[0].play();
        } else {
            $('#fullvideo')[0].pause();
        }
    });

    /* Cambio l'icona del volume */
    $('#fullvideo').on('volumechange', function (e) {
        if ($(this).prop('muted')) {
            $('#fullvideo-video-mute .fa-ban').removeClass('hidden');
        } else {
            $('#fullvideo-video-mute .fa-ban').addClass('hidden');
        }
    });

    /* Cambio l'icona della pausa */
    $('#fullvideo').on('pause', function (e) {
        $('#fullvideo-video-pause .fa').removeClass('fa-pause');
        $('#fullvideo-video-pause .fa').addClass('fa-play');
    });

    $('#fullvideo').on('play', function (e) {
        $('#fullvideo-video-pause .fa').removeClass('fa-play');
        $('#fullvideo-video-pause .fa').addClass('fa-pause');
    });

    /* Apro il video a tutto schermo */

    if ($('.boxer-video').length) {
        boxerVideo = $(".boxer-video");
        boxerVideo.lightbox({
            mobile: true,
            labels: {
                close: 'Chiudi',
                count: 'di',
                next: 'Successivo',
                previous: 'Precedente',
                captionClosed: 'Descrizione',
                captionOpen: 'Descrizione'
            }
        });
    }

    $(window).on('open.lightbox', function () {
        if ($('#fullvideo').length) {
            if (!$('#fullvideo').prop('paused')) {
                $('#fullvideo').addClass('force-paused');
            }
            $('#fullvideo')[0].pause();
        }
    });

    $(window).on('close.lightbox', function () {
        if ($('#fullvideo').hasClass('force-paused')) {
            $('#fullvideo')[0].play();
            $('#fullvideo').removeClass('force-paused');
        }

    });

});

/*
 * Menu Hoverintent
 */


if ($('#menu1-wrapper .menu-item').length) {

    $('#menu1-wrapper .menu-item').hoverIntent({
        over: menuIntentOpen,
        timeout: 150,
        out: $.noop
    });
    $('#menu1-wrapper').hoverIntent({
        over: $.noop,
        out: menuIntentClose
    });

}

/*
 * Waypoints
 */
if ($('#aggiornamenti').length) {
    $('#aggiornamenti').waypoint(function () {
        docCookies.setItem('newtime', (new Date).getTime(), 31536e3, '/');
        console.log('set time');
    }, {
        offset: '75%'
    });
}

/*
 * Smooth scroll
 */

if ($('#aggiornamenti').length) {
    $('a[href=#aggiornamenti]').attr('data-scroll', 1);
    smoothScroll.init({
        speed: 500,
        easing: 'easeOutQuad',
        updateURL: false,
        offset: 60,
        callback: function (toggle, anchor) {
            if ((anchor == '#aggiornamenti') && $('body').hasClass('fs-navigation-lock')) {
                $("#mbNav").navigation("close");
            }
        }
    });
}

if (typeof smoothScroll == 'object') {
    smoothScroll.init();
}

/*
 * Lightbox (ex Boxer)
 */
if (ie8 === false && $('.gallery-icon').length) {
    $('.gallery-icon a').each(function (index) {
        $(this).attr('data-lightbox-gallery', 'gallery');
    });

    var lightboxOpts = {
        mobile: true,
        labels: {
            close: 'Chiudi',
            count: 'di',
            next: 'Successivo',
            previous: 'Precedente',
            captionClosed: 'Descrizione',
            captionOpen: 'Descrizione'
        }
    };

    $(".gallery-icon a").lightbox(lightboxOpts);

    $(".wp-caption a").lightbox(lightboxOpts);
}


/*
 * Shifter
 */
if (ie8 === false) {
    $("#mbNav").navigation({
        type: 'overlay',
        label: false,
        gravity: 'right'
    });
}

/*
 * Hypenator
 */
if (typeof Hypher != 'undefined') {
    if ($('.calendar-inner-card  p.event-content').length) {
        $('p.event-content, p.event-content strong, p.event-content em, p.event-content i, p.event-content b').hyphenate('it');
    }
    if ($('.post-bodycopy p').length) {
        $('.post-bodycopy p').hyphenate('it');
    }
    if ($('#inevidenza p').length) {
        $('#inevidenza p').hyphenate('it');
    }
    if ($('.post-bodycopy h2').length) {
        $('.post-bodycopy h2').hyphenate('it');
    }
    if ($('.post-bodycopy h3').length) {
        $('.post-bodycopy h3').hyphenate('it');
    }
}

/**
 * Altezza delle card
 */


/**
 * Imposto l'altezza delle card
 */
function isAppuntamentiShowed() {
    if ($('.calendar-card h3').height() > 25) {
        dataLayer.push({
            'timingCategory': 'Dipendenze',
            'timingVar': 'Full CSS Load',
            'timingValue': performance.now(),
            'timingLabel': 'CSS Load',
            'event': 'timeTrack'
        });
        console.log('Calendar height ok: ' + $('.calendar-card h3').height());
        return true;
    } else {
        console.log('Calendar height ko: ' + $('.calendar-card h3').height());
        return false;
    }
}


function resizeCard() {
    $('.grid-paragraph').each(function (index) {
        var card = $(this).attr('data-card');
        if (!card) {
            console.log('ritorno');
            return;
        }
        var height = $(this).height();
        console.log('height=' + height);
        if (!window[card]) {
            window[card] = 0;
        }
        window[card] = height > window[card] ? height : window[card];
    });
    $('.grid-paragraph').each(function (index) {
        var card = $(this).attr('data-card');
        console.log('imposto ' + card + ' all\'altezza di ' + window[card]);
        $(this).height(window[card]);
    });
}

function runResizeCard() {
    $(document).ready(function () {
        console.log('eseguo resize delle card');
        if ($('body.home').length) {
            waitfor(isAppuntamentiShowed, true, 100, 0, 'css loaded and executed', resizeCard);
        } else {
            resizeCard();
        }
    });
}

/*
 * EnquireJS
 */


if (ie8 === false) {
    $.mediaquery("bind", "mq-xs", "screen and (max-width: 767px)", {
        enter: function () {
            console.log('enter screen and (max-width: 767px)');
            runTooltip('mobile');
            runLabel('left');
            runDownloadButton();
        }
    });
    $.mediaquery("bind", "mq-sm", "screen and (min-width: 768px) and (max-width: 991px)", {
        enter: function () {
            console.log('enter screen and (min-width: 768px) and (max-width: 991px)');
            runTooltip('mobile');
            runLabel('left');
            runResizeCard();
            runDownloadButton();
        }
    });
    $.mediaquery("bind", "mq-md", "screen and (min-width: 992px) and (max-width: 1199px)", {
        enter: function () {
            console.log('enter screen and (min-width: 992px) and (max-width: 1199px)');
            if (($(document).height() - $(window).height()) > 275) {
                $('#main-menu').waypoint('sticky');
            }
            runLabel('right');
            runResizeCard();
            runDownloadButton();
        }

    });
    $.mediaquery("bind", "mq-lg", "screen and (min-width: 1200px)", {
        enter: function () {
            console.log('enter: screen and (min-width: 1200px)');
            if (($(document).height() - $(window).height()) > 275) {
                $('#main-menu').waypoint('sticky');
            }
            runLabel('right');
            runTooltip('desktop');
            runResizeCard();
            runDownloadButton();
        }

    });
} else {
    console.log('è IE8');
    if (($(document).height() - $(window).height()) > 275) {
        $('#main-menu').waypoint('sticky');
        runLabel('left');
    }
}