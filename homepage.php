<?php
$numberposts = 15;

$query00 = array(
	'numberposts' => $numberposts,
	'post_type'   => array( 'post', 'circolare', 'avviso' ),
);
$query01 = array(
	'numberposts'   => $numberposts,
	'post_type'     => array( 'post', 'circolare', 'avviso' ),
	'category_name' => 'docenti-ata',
	'meta_query'  => array(
		'relation' => 'OR',
		array(
			'key'     => 'avviso_num',
			'compare' => 'NOT EXISTS',
		),
		array(
			'key'     => 'avviso_num',
			'value'   => 'interno',
			'compare' => '!=',
		),
	)
);
$query02 = array(
	'numberposts'   => $numberposts,
	'post_type'     => array( 'post', 'circolare', 'avviso' ),
	'category_name' => 'famiglie',
	'meta_query'  => array(
		'relation' => 'OR',
		array(
			'key'     => 'avviso_num',
			'compare' => 'NOT EXISTS',
		),
		array(
			'key'     => 'avviso_num',
			'value'   => 'interno',
			'compare' => '!=',
		),
	)
);
$query03 = array(
	'numberposts'   => $numberposts,
	'post_type'     => array( 'post', 'circolare', 'avviso' ),
	'category_name' => 'studenti',
	'meta_query'  => array(
		'relation' => 'OR',
		array(
			'key'     => 'avviso_num',
			'compare' => 'NOT EXISTS',
		),
		array(
			'key'     => 'avviso_num',
			'value'   => 'interno',
			'compare' => '!=',
		),
	)
);
$query04 = array(
	'numberposts'  => $numberposts,
	'post_type'    => array( 'avviso' ),
	'meta_key'     => 'avviso_num',
	'meta_value'   => 'interno',
	'meta_compare' => '='
);


get_header();
?>
<?php
get_template_part( 'home', 'hp' );
?>
<?php
if ( ot_get_option( 'hp_type' ) == 'youtube' && ot_get_option( 'yt_iframe' ) != null ) {
	get_template_part( 'home', 'youtube' );
} elseif ( ot_get_option( 'hp_type' ) == 'set_hp_html_code' && ot_get_option( 'hp_html_code' ) != null ) {
	get_template_part( 'home', 'html_code' );
} elseif ( ot_get_option( 'hp_type' ) == 'zaccordion' ) {
	get_template_part( 'home', 'pinterest' );
} elseif ( ot_get_option( 'hp_type' ) == 'null' ) {

}
?>


<?php
if ( ot_get_option( 'in_evidenza_show', 'no' ) == 'yes' ) {
	get_template_part( 'home', 'inevidenza' );
}
?>


<?php
if ( ot_get_option( 'fastlink_show', 'no' ) == 'yes' ) {
	get_template_part( 'home', 'appuntamenti' );
}
if ( ot_get_option( 'fastlink_show', 'no' ) == 'dynamic' ) {
	get_template_part( 'home', 'appuntamenti-dynamic' );
}
?>


<?php
if ( ot_get_option( 'certificazioni_show', 'no' ) == 'yes' ) {
	get_template_part( 'home', 'certificazioni' );
}
?>

<?php
if ( ot_get_option( 'partner_show', 'no' ) == 'yes' ) {
	get_template_part( 'home', 'didattica' );
}
?>

<?php
if ( ot_get_option( 'partner_show', 'no' ) == 'yes' ) {
	get_template_part( 'home', 'partner' );
}
?>


    <div id="aggiornamenti" class="tabbed col-md-8 bd-right clearfix" style="z-index: 1;">
        <h1 class="title compensate-bs">Ultimi aggiornamenti</h1>
        <div id="tab-container" class="tab-container " role="tabpanel">
            <ul class='nav nav-pills nav-justified' style="width:inherit">
                <li role="presentation" class="active"><a href="#tutto" aria-controls="tutto" role="tab"
                                                          data-toggle="tab">Tutti</a></li>
                <li role="presentation"><a href="#docenti" aria-controls="docenti" role="tab" data-toggle="tab">Docenti&nbsp;e&nbsp;ATA</a>
                </li>
                <li role="presentation"><a href="#studenti" aria-controls="studenti" role="tab" data-toggle="tab">Studenti</a>
                </li>
                <li role="presentation"><a href="#famiglie" aria-controls="famiglie" role="tab" data-toggle="tab">Famiglie</a>
                </li>
                <li role="presentation"><a href="#interni" aria-controls="interni" role="tab" data-toggle="tab">Comunicazioni
                        interne</a>
                </li>
            </ul>
            <div class="tab-content">
                <div id="tutto" role="tabpanel" class="tab-pane fade in active data-scroll-append">
					<?php
					$mypost = get_posts( $query00 );
					if ( $mypost ) {
						foreach ( $mypost as $post ) : setup_postdata( $post );
							get_template_part( 'acerbo', 'loop' );
						endforeach;
						wp_reset_postdata();
					}
					?>
                    <nav>
                        <ul class="pager">
                            <li><a href="/novita/page/2/" data-tmpl="loop" class="data-scroll"
                                   data-pt="circolare,avviso,post" data-offset="<?php echo $numberposts - 1; ?>">Visualizza
                                    aggiornamenti precedenti</a></li>
                        </ul>
                    </nav>
                </div>
                <div id="docenti" role="tabpanel" class="tab-pane data-scroll-append fade">
					<?php
					$mypost = get_posts( $query01 );
					if ( $mypost ) {
						foreach ( $mypost as $post ) : setup_postdata( $post );
							get_template_part( 'acerbo', 'loop' );
						endforeach;
						wp_reset_postdata();
					}
					?>
                    <nav>
                        <ul class="pager">
                            <li><a href="/categoria/docenti-ata/page/2" data-tmpl="loop" class="data-scroll"
                                   data-cat="docenti" data-pt="circolare,avviso,post"
                                   data-offset="<?php echo $numberposts - 1; ?>">Visualizza aggiornamenti precedenti</a>
                            </li>
                        </ul>
                    </nav>
                </div>
                <div id="studenti" role="tabpanel" class="tab-pane data-scroll-append fade">
					<?php
					$mypost = get_posts( $query02 );
					if ( $mypost ) {
						foreach ( $mypost as $post ) : setup_postdata( $post );
							get_template_part( 'acerbo', 'loop' );
						endforeach;
						wp_reset_postdata();
					}
					?>
                    <nav>
                        <ul class="pager">
                            <li><a href="/categoria/studenti/page/2" data-tmpl="loop" class="data-scroll"
                                   data-cat="studenti" data-pt="circolare,avviso,post"
                                   data-offset="<?php echo $numberposts - 1; ?>">Visualizza aggiornamenti precedenti</a>
                            </li>
                        </ul>
                    </nav>
                </div>
                <div id="famiglie" role="tabpanel" class="tab-pane data-scroll-append fade">
					<?php
					$mypost = get_posts( $query03 );
					if ( $mypost ) {
						foreach ( $mypost as $post ) : setup_postdata( $post );
							get_template_part( 'acerbo', 'loop' );
						endforeach;
						wp_reset_postdata();
					}
					?>
                    <nav>
                        <ul class="pager">
                            <li><a href="/categoria/famiglie/page/2" data-tmpl="loop" class="data-scroll"
                                   data-cat="famiglie" data-pt="circolare,avviso,post"
                                   data-offset="<?php echo $numberposts - 1; ?>">Visualizza aggiornamenti precedenti</a>
                            </li>
                        </ul>
                    </nav>
                </div>
                <div id="interni" role="tabpanel" class="tab-pane data-scroll-append fade">
		            <?php
		            $mypost = get_posts( $query04 );
		            if ( $mypost ) {
			            foreach ( $mypost as $post ) : setup_postdata( $post );
				            get_template_part( 'acerbo', 'loop' );
			            endforeach;
			            wp_reset_postdata();
		            }
		            ?>
                    <nav>
                        <ul class="pager">
                            <li><a href="/avviso/page/2/" data-tmpl="loop" class="data-scroll"
                                   data-key="avviso_num" data-value="interno" data-pt="avviso"
                                   data-offset="<?php echo $numberposts - 1; ?>">Visualizza aggiornamenti precedenti</a>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>

    </div>

    <div id="widgetarea-one" class="col-md-4">
        <h1 class="title compensate-bs" style="margin-bottom: 25px">Sezioni</h1>
		<?php dynamic_sidebar( 'sidebar-2' ); ?>
    </div>


<?php get_footer(); ?>