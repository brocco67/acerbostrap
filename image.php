   
<?php get_header(); ?>

    <div id="content" class="col-md-12">   

        <?php the_post(); ?>
        <div id="post-<?php the_ID(); ?>" <?php post_class('cf image-attachment'); ?>>

            <h1 class="title compensate-bs">
                <?php the_title(); ?>
            </h1>

            <div class="blocchetto">
                <div class="post-bodycopy cf">

                    <div class="wp-caption">
                        <a href="<?php bfa_attachment_url(); ?>"><?php bfa_attachment_image('full'); ?></a>
                        <?php bfa_attachment_caption(); ?>
                    </div>

                    <nav class="singlenav cf">
                        <div class="older"><?php previous_image_link(false); ?></div>
                        <div class="newer"><?php next_image_link(false); ?></div>
                    </nav>				

                    <div class="entry-description">
                        <?php the_content(); ?>
                        <?php wp_link_pages(array('before' => '<div class="page-links">' . __('Pages:', 'montezuma'), 'after' => '</div>')); ?>
                    </div>

                </div>
                
                        <div class="post-footer">
                            <?php
                           echo '<span><i class="fa fa-clock-o"></i>&nbsp;Immagine pubblicata il ' . get_the_time('j F Y') . ' e collegata alla pagina <a href="'.bfa_parent_permalink($echo = false).'">'.bfa_parent_title($echo = false).'</a></span>';
                            ?>
                        </div>

                <?php edit_post_link('Modifica questa pagina'); ?>

                <div class="post-footer">
                    <p><?php bfa_image_meta(); ?></p>

                </div>

            </div>

            <?php comments_template(); ?>
        </div>
    </div>

<?php get_footer(); ?>