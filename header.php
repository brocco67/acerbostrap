<?php
if ( function_exists( 'ot_get_option' ) ) {
	$bg_image         = ot_get_option( 'bg_image', array() );
	$bg_avviso        = is_front_page() ? 'default-bg' : 'dotted';
	$acerbo_facebook  = ot_get_option( 'acerbo_facebook', '' );
	$acerbo_twitter   = ot_get_option( 'acerbo_twitter', '' );
	$acerbo_youtube   = ot_get_option( 'acerbo_youtube', '' );
	$acerbo_instagram = ot_get_option( 'acerbo_instagram', '' );
}
?>
    <!DOCTYPE html>
    <!--[if lt IE 7]>
    <html class="no-js lt-ie9 lt-ie8 lt-ie7" <?php language_attributes(); ?>> <![endif]-->
    <!--[if IE 7]>
    <html class="no-js lt-ie9 lt-ie8" <?php language_attributes(); ?>> <![endif]-->
    <!--[if IE 8]>
    <html class="no-js lt-ie9" <?php language_attributes(); ?>> <![endif]-->
    <!--[if gt IE 8]><!-->
<html class="no-js" <?php language_attributes(); ?>> <!--<![endif]-->
    <head>
        <meta charset="<?php bloginfo( 'charset' ); ?>"/>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <link rel="dns-prefetch" href="https://istitutoacerbo.appspot.com/">
		<?php
		if ( is_front_page() ) {
			echo '<title>' . get_bloginfo( 'name' ) . ' | ' . get_bloginfo( 'description' ) . '</title>';
			echo '<meta name="description" content="La scuola superiore più antica di Pescara che dal 1923 garantisce a tutti gli studenti la qualità della formazione nel rispetto della tradizione."/>';
		} else if ( is_attachment() ) {
			echo '<title>[ALLEGATO] ' . wp_title( '|', false, 'right' ) . ' ' . get_bloginfo( 'name' ) . '</title>';
			echo '<meta name="description" content="' . strip_tags( get_the_excerpt() ) . '" />';
		} else if ( is_single() || is_page() ) {
			echo '<title>' . wp_title( '|', false, 'right' ) . ' ' . get_bloginfo( 'name' ) . '</title>';
			setup_postdata( $post );
			echo '<meta name="description" content="' . strip_tags( get_the_excerpt() ) . '" />';
		} else {
			echo '<title>' . wp_title( '|', false, 'right' ) . ' ' . get_bloginfo( 'name' ) . '</title>';
		}
		?>
        <meta name="apple-mobile-web-app-title" content="ITS Acerbo">
        <link rel="apple-touch-icon"
              href="<?php echo get_template_directory_uri() . '/images/apple-touch-icon-precomposed.png'; ?>"/>
        <link rel="apple-touch-icon" sizes="72x72"
              href="<?php echo get_template_directory_uri() . '/images/apple-touch-icon-72x72-precomposed.png'; ?>"/>
        <link rel="apple-touch-icon" sizes="114x114"
              href="<?php echo get_template_directory_uri() . '/images/apple-touch-icon-114x114-precomposed.png'; ?>"/>
        <link rel="apple-touch-icon" sizes="144x144"
              href="<?php echo get_template_directory_uri() . '/images/apple-touch-icon-144x144-precomposed.png'; ?>"/>
        <link rel="apple-touch-startup-image"
              href="<?php echo get_template_directory_uri() . '/images/startup.png'; ?>"/>
        <link rel="shortcut icon" sizes="320x320"
              href="<?php echo get_template_directory_uri() . '/images/acerbo-icon-320.png'; ?>"/>
        <link rel="shortcut icon" sizes="144x144"
              href="<?php echo get_template_directory_uri() . '/images/acerbo-icon-144.png'; ?>"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
        <meta name="application-name" content="Istituto Acerbo"/>
        <meta name="msapplication-TileColor" content="#ffffff"/>
        <meta name="msapplication-square70x70logo" content="images/tiles/tiny.png"/>
        <meta name="msapplication-square150x150logo" content="images/tiles/square.png"/>
        <meta name="msapplication-wide310x150logo" content="images/tiles/wide.png"/>
        <meta name="msapplication-square310x310logo" content="images/tiles/large.png"/>
        <meta name="msapplication-notification"
              content="frequency=30;polling-uri=http://notifications.buildmypinnedsite.com/?feed=http://www.istitutotecnicoacerbope.edu.it/feed/&amp;id=1;polling-uri2=http://notifications.buildmypinnedsite.com/?feed=http://www.istitutotecnicoacerbope.edu.it/feed/&amp;id=2;polling-uri3=http://notifications.buildmypinnedsite.com/?feed=http://www.istitutotecnicoacerbope.edu.it/feed/&amp;id=3;polling-uri4=http://notifications.buildmypinnedsite.com/?feed=http://www.istitutotecnicoacerbope.edu.it/feed/&amp;id=4;polling-uri5=http://notifications.buildmypinnedsite.com/?feed=http://www.istitutotecnicoacerbope.edu.it/feed/&amp;id=5; cycle=1"/>
		<?php
		$short = acerbo_get_short( get_the_ID() );
		if ( $short ) {
			echo '<meta property="ac:shorturl" content="' . $short . '" />';
		}
		?>

        <script>
            var a = document.getElementsByTagName("html")[0];
            a.className && (
                a.className = a.className.replace(/no-js\s?/, '')
            );
        </script>
		<?php
		global $developer;
		if ( $developer != 'yes' ) {
			include_once get_template_directory() . '/build/critical/critical-css.php';
		}
		?>
        <!--[if IE 9 ]>
        <script src="/wp-content/themes/acerbostrap/build/js/matchMediaIE9.min.js"></script>
        <![endif] -->
		<?php wp_head(); ?>
		<?php get_template_part( 'datalayer' ); ?>
        <!--[if lt IE 9]>
        <link rel="stylesheet" href="/wp-content/themes/acerbostrap/build/css/all.condensed.min.css"/>
        <?php if ( is_front_page() ) {
			echo '<link rel="stylesheet" href="/wp-content/themes/acerbostrap/build/css/owl.condensed.min.css" />';
        } ?>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
        <script src="http://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="http://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <script src="/wp-content/themes/acerbostrap/assets/js/modernizr.custom.74916.js"></script>
        <script src="/wp-content/themes/acerbostrap/build/js/blockingie8.min.js"></script>
        <script>
            if (!window.console) {
                console = {
                    log: function () {
                    }
                }
            }
        </script>
        <![endif] -->
    </head>
<body <?php body_class( 'acerbostrap' ); ?>>
    <!-- Google Tag Manager -->
    <noscript>
        <iframe src="//www.googletagmanager.com/ns.html?id=GTM-NR8MRH"
                height="0" width="0" style="display:none;visibility:hidden"></iframe>
    </noscript>
    <script>(
            function (w, d, s, l, i) {
                w[l] = w[l] || [];
                w[l].push({
                    'gtm.start': new Date().getTime(), event: 'gtm.js'
                });
                var f = d.getElementsByTagName(s)[0],
                    j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : '';
                j.async = true;
                j.src =
                    '//www.googletagmanager.com/gtm.js?id=' + i + dl;
                f.parentNode.insertBefore(j, f);
            }
        )(window, document, 'script', 'dataLayer', 'GTM-NR8MRH');</script>
    <!-- End Google Tag Manager -->

    <nav class="navbar navbar-default navbar-fixed-top visible-xs-block" role="navigation" id="navbarTop">
        <div class="container">
            <div class="navbar-header">
                <a data-toggle="tooltip" data-placement="left" data-trigger="manual" title="Menu"
                   class="nav_handle mobile_handle shifter-handle">
                    <span class="sr-only">Apri il menu</span>
                </a>
				<?php if ( is_front_page() ) { ?>
                    <span class="navbar-brand"><?php bloginfo( 'name' ); ?></span>
				<?php } else { ?>
                    <a class="navbar-brand" href="<?php echo home_url(); ?>"><?php bloginfo( 'name' ); ?></a>
				<?php } ?>
            </div>

        </div>
    </nav>
<div id="mbAll">
    <!--[if lt IE 11]>
    <div class="alert alert-warning" role="alert"><strong>Attenzione</strong> Stai utilizzando un browser <strong>non
        più aggiornato</strong> e questo sito potrebbe essere visualizzato in modo disomogeneo. Il browser che utilizzi
        <strong>non è sicuro</strong> poiché non viene più aggiornato e navigare con esso è pericoloso. Inoltre, non
        supporta molte delle funzionalità dei moderni siti web. Ti consigliamo caldamente di <a
                href="http://whatbrowser.org/?hl=it">aggiornare il browser</a> o di chiedere l'aggiornamento
        all'amministratore del sistema: renderai più sicura la tua vita online e migliorerai la navigazione di questo e
        di tutti gli altri siti.
    </div>
    <![endif]-->
    <div id="mbCont" class="shifter-page">

    <!--div id="christmas" class="container cf maxistop hidden-print" ></div-->

    <div id="avviso" class="container cf maxistop <?php echo $bg_avviso; ?> hidden-print"
         style="height: 0px;overflow: hidden;"></div>


    <div id="header" class="container cf maxistop hidden-xs <?php
	if ( ! is_front_page() ) {
		echo 'dotted';
	}
	?>">
        <div class="row cf">
            <div class="col-sm-12 col-md-7 headouter">
                <div id="headline" class="headinner">
                    <h1 id="sitetitle">
						<?php if ( is_front_page() ) { ?>
							<?php bloginfo( 'name' ); ?>
						<?php } else { ?>
                            <a href="<?php echo home_url(); ?>"><?php bloginfo( 'name' ); ?></a>
						<?php } ?>
                    </h1>

                    <p id="tagline"><?php bloginfo( 'description' ); ?></p>
                </div>
            </div>
            <div id="headmenu" class="col-sm-12 col-md-5">
                <nav id="menu2-wrapper" class="menu-menu-secondario-container">
                    <ul id="menu2" class="cf menu">
                        <li id="menu-item-1789"
                            class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1789">
                            <a href="/scuola/urp/">URP</a>
                        </li>

                        <li id="menu-item-1022"
                            class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1022">
                            <a target="_blank"
                               href="http://www.trasparenza-pa.net/?codcli=SG19032">Amministrazione Trasparente</a>
                        </li>
                        <li id="menu-item-1023"
                            class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1023">
                            <a target="_blank" href="https://www.albipretorionline.com/SG19032">Albo Pretorio</a>
                        </li>
                        <li id="menu-item-1023"
                            class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1023">
                            <a href="/bacheca-sindacale/">Bacheca sindacale</a>
                        </li>
                        <li id="menu-item-1023" class="menu-item menu-item-type-custom menu-item-object-custom">
                            <a href="/privacy/">Privacy</a>
                        </li>
                    </ul>
                </nav>
				<?php
				/*wp_nav_menu( array(
					'container'       => 'nav',
					'container_class' => '',
					'container_id'    => 'menu2-wrapper',
					'menu_id'         => 'menu2',
					'menu_class'      => 'cf menu',
					'theme_location'  => 'menu2',
					'fallback_cb'     => false,
					'walker'          => new Child_Wrap()
				) );*/
				?>
				<?php
				if ( ! is_search() ) {
					get_search_form();
				}
				?>
            </div>
        </div>
    </div>
<?php get_template_part( 'main', 'menu' ); ?>
<?php
if ( is_front_page() ) {
	get_template_part( 'home', 'slider' );
}
?>


<?php if ( ! is_front_page() ) { ?>
    <div class="bread dotted maxistop">

		<?php if ( function_exists( 'bcn_display' ) ) { ?>
            <div id="breadcrumbs1-bg">
                <nav id="breadcrumbs1" class="breadcrumbs lw">
                    <ol class="list-inline">
						<?php bcn_display();
						?>
                    </ol>
                </nav>
            </div>
		<?php }
		?>

        <div id="breadcrumbs-follow" class="social-follow hidden-xs">
                            <span>
                                <?php
                                if ( $acerbo_facebook ) {
	                                echo '<a href="https://www.facebook.com/' . $acerbo_facebook . '" title="Seguici su Facebook" target="_blank"><i class="fa fa-facebook-square fa-2x"></i></a>';
                                }
                                if ( $acerbo_twitter ) {
	                                echo '<a href="https://www.twitter.com/' . $acerbo_twitter . '" title="Seguici su Twitter" target="_blank"><i class="fa fa-twitter-square fa-2x"></i></a>';
                                }
                                if ( $acerbo_youtube ) {
	                                echo '<a href="https://www.youtube.com/user/' . $acerbo_youtube . '" title="Seguici su Youtube" target="_blank"><i class="fa fa-youtube-square fa-2x"></i></a>';
                                }
                                if ( $acerbo_instagram ) {
	                                echo '<a href="https://www.instagram.com/' . $acerbo_instagram . '/?hl=it" title="Seguici su Instagram" target="_blank"><i class="fa fa-instagram fa-2x"></i></a>';
                                }
                                echo '<a href="/feed" title="Seguici via RSS" target="_blank"><i class="fa fa-rss-square fa-2x"></i></a>';
                                ?>
                            </span>
        </div>
    </div>
<?php } ?>
    <div class="container">
        <div id="main" class="row dotted">
<?php
if ( ! is_page( 5922 ) ) {
//	get_template_part( 'extra', 'banner-orientamento' );
}
