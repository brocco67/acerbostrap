<?php
get_header();
$title   = $post->post_title;
$content = $post->guid;
?>

    <div id="content" class="col-md-12">
<?php while ( have_posts() ) : the_post(); ?>
    <div id="post-<?php the_ID(); ?>">

        <h1 class="title compensate-bs">
			<?php echo str_replace( '_', ' ', $title ); ?>
        </h1>

		<?php
		if ( has_post_thumbnail() ) {
			echo '<div class="post-thumb"><div class="thumb-inner">';
			the_post_thumbnail( 'showed' );
			echo '</div></div>';
		}
		?>
        <div class="post-bodycopy cf">

			<?php
			if ( $post->post_mime_type != 'image' ) {

				$parse    = parse_url( $content );
				$filePath = $parse['path'];
				$randomId = substr( str_shuffle( "abcdefghijklmnopqrstuvwxyz" ), 0, 6 );

				$title  = '<h3 class="doctitle"><span class="fa-stack"><i class="fa fa-square fa-stack-2x fa-color-pdf"></i><i class="fa fa-file-pdf-o fa-stack-1x fa-inverse"></i></span>&nbsp;&nbsp;<a href="' . $content . '" target="_blank">' . str_replace( '_', ' ', $title ) . '</a><span class="hidden-xs doc__button pull-right"><a href="http://drive.google.com/viewerng/viewer?url=' . $content . '" target="_blank" class="nofancybox"><button type="button" class="btn btn-xs" data-toggle="tooltip" data-placement="bottom" title="Visualizza in una nuova finestra"><i class="fa fa-external-link"></i></button></a>'
				          . '<a href="' . $content . '" target="_blank" class="nofancybox" title="Scarica il file" download><button type="button" class="btn btn-xs htmldownload"><i class="fa fa-download" data-toggle="tooltip" data-placement="bottom" title="Scarica il file"></i></button></a>'
				          . '<a href="/download.php?dir=' . ltrim( dirname( $filePath ), '/' ) . '&file=' . basename( $filePath ) . '" class="nofancybox" title="Scarica il file"><button type="button" class="btn btn-xs phpdownload" data-toggle="tooltip" data-placement="bottom" title="Scarica il file"><i class="fa fa-download"></i></button></a></span></h3>';
				$iframe = '<div id="' . $randomId . '" class="docframe embed-responsive embed-responsive-4by3"></div>';

				echo $title . $iframe;
				echo '<script>function defer(method){if(window.$)method();else setTimeout(function(){defer(method)},50)}defer(function(){jQuery(document).ready(function($){$("a.getbox").trigger("click")})});</script>';
			}
			?>


            <div style="padding: 20px 0 0 0">
                <h2>Analisi dei tag</h2>
				<?php the_content(); ?>
            </div>


			<?php
			wp_link_pages( array(
				'before' => __( '<p class="post-pagination">Pages:', 'montezuma' ),
				'after'  => '</p>'
			) );
			?>
        </div>

    </div>
    </div>
<?php endwhile; ?>


<?php get_footer(); ?>