<?php
// Cerco i post-meta
$date = date( 'U' );
if ( is_front_page() ) {
	$sticked = '';
} else {
	$sticked = ( date( "U", strtotime( CFS()->get( 'sticked' ) ) ) > $date ) ? 'well' : '';
}


if ( $post->post_type == 'circolare' ) {
	$num = CFS()->get( 'circolare_num' );
}

if ( $post->post_type == 'avviso' ) {
	$num = CFS()->get( 'avviso_num' );
}

$permalink = urlencode( get_permalink() );
?>
<div class="acerbo__loop <?php echo $sticked; ?>">
	<h2 data-updated="<?php the_time( 'U' ); ?>">
		<?php
		if ( $post->post_type == 'circolare' ) {
			echo '<a href="' . get_the_permalink() . '" rel="bookmark" title="Circolare ' . $num . '">' . get_the_title() . '</a>';
		} elseif ( $post->post_type == 'avviso' ) {
			echo '<a href="' . get_the_permalink() . '" rel="bookmark" title="Avviso, ' . $num . '">' . get_the_title() . '</a>';
		} else {
			echo '<a href="' . get_the_permalink() . '" rel="bookmark">' . get_the_title() . '</a>';
		}
		?>

	</h2>

	<div class="post__loop cf">
		<div class="post__loop--date">
			<?php
			if ( $post->post_type == 'circolare' ) {
				$date  = CFS()->get( 'circolare_data' );
				$day   = date_i18n( 'j', strtotime( $date ) );
				$month = date_i18n( 'M', strtotime( $date ) );
				$year  = date_i18n( 'Y', strtotime( $date ) );
			} elseif ( $post->post_type == 'avviso' ) {
				$date  = CFS()->get( 'avviso_data' );
				$day   = date_i18n( 'j', strtotime( $date ) );
				$month = date_i18n( 'M', strtotime( $date ) );
				$year  = date_i18n( 'Y', strtotime( $date ) );
			} else {
				$day   = get_the_time( 'j' );
				$month = get_the_time( 'M' );
				$year  = get_the_time( 'Y' );
			}
			?>
			<p class="post-day"><?php echo $day; ?></p>
			<p class="post-month"><?php echo $month; ?></p>
			<p class="post-year"><?php echo $year; ?></p>
		</div>
		<div class="post__loop--excerpt">
			<?php
			if ( $post->post_type == 'circolare' ) {
				echo '<span class="circolare-loop"><i class="fa fa-file-o" style="font-size: 15px;"></i>&nbsp;&nbsp;Circolare ' . $num . '</span>';
			} elseif ( $post->post_type == 'avviso' ) {
				echo '<span class="circolare-loop"><i class="fa fa-file-o" style="font-size: 15px;"></i>&nbsp;&nbsp;Avviso, ' . $num . '</span>';
			}
			?>
			<?php
			if ( acerbo_empty_content($post->post_content) ) {
				echo '<p>' . get_the_title() . '</p>';
			} else {
				the_excerpt();
			}
			?>
			<div class="post-share">
				<span class="hidden-xs">Condividi:</span>
                <a href="https://telegram.me/share/url?url=<?php echo $permalink; ?>&text=<?php echo urlencode( get_the_title() ); ?>"
                   title="Condividi su Telegram" class="wn-pop share-link visible-xs-inline-block"><i
                            class="fa fa-telegram fa-lg icon-telegram"></i><span class="sr-only">Condividi su Telegram</span></a>
				<a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo $permalink; ?>"
				   title="Condividi su Facebook" class="wn-pop share-link"><i
						class="fa fa-facebook fa-lg icon-facebook" aria-hidden="true"></i><span class="sr-only">Condividi su Facebook</span></a>
				<a href="https://twitter.com/share?url=<?php echo $permalink; ?>&via=acerbosocial&text=<?php echo urlencode( get_the_title() ); ?>"
				   title="Condividi su Twitter" class="wn-pop share-link"><i
						class="fa fa-twitter fa-lg icon-twitter" aria-hidden="true"></i><span class="sr-only">Condividi su Twitter</span></a>
				<a href="<?php echo get_the_permalink(); ?>"
				   data-title="<?php echo urlencode( get_the_title() ); ?>"
				   data-shorturl="<?php echo acerbo_get_short( $post->ID ); ?>" title="Condividi su Whatsapp"
				   class="whatsapp-share share-link visible-xs-inline-block"><i
						class="fa fa-whatsapp fa-lg icon-whatsapp" aria-hidden="true"></i><span class="sr-only">Condividi su Whatsapp</span></a>

			</div>
		</div>
	</div>
</div>