<?php
if ( function_exists( 'ot_get_option' ) ) {
	$output = ot_get_option( 'hp_text', '' );
}
if ( $output ) :
	?>

    <div id="hp" class="fastlink col-sm-12">
        <h1 class="title compensate-bs">Novità importanti</h1>
        <div class="row fastlink">
            <div class="hp_text">
				<?php
				echo $output;
				?>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>

	<?php
endif;
?>