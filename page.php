<?php
get_header();
$shortoutput = '';
?>

<div id="content" class="col-md-8 bd-right">

    <?php while (have_posts()) : the_post(); ?>

        <?php
        if (has_shortcode(get_the_content(), 'showloop')) {

            $pattern = '\[(\[?)(showloop)(?![\w-])([^\]\/]*(?:\/(?!\])[^\]\/]*)*?)(?:(\/)\]|\](?:([^\[]*+(?:\[(?!\/\2\])[^\[]*+)*+)\[\/\2\])?)(\]?)';
            preg_match('/' . $pattern . '/s', $post->post_content, $matches);
            if (is_array($matches) && isset($matches[2])) {
                if ($matches[2] == 'showloop') {
                    $shortcode = $matches[0];
                    $shortoutput = do_shortcode($shortcode);
                }
            }
            remove_shortcode('showloop');
            add_shortcode('showloop', '__return_false');
        }
        ?>


        <div id="post-<?php the_ID(); ?>" <?php post_class('acerbo-entry'); ?>>
            <?php
            if (has_post_thumbnail()) {
                echo '<div class="post-thumb compensate-bs"><div class="thumb-inner">';
                the_post_thumbnail('showed');
                echo '</div></div>';
            }
            ?>
            <div class="page-header">
                <h1 class="title compensate-bs">
                    <?php the_title(); ?>
                </h1>
            </div>

            <div class="post-bodycopy">
                <?php the_content(); ?>
                <?php
                wp_link_pages(array(
                    'before' => __('<p class="post-pagination">Pages:', 'montezuma'),
                    'after' => '</p>'
                ));
                ?>
            </div>

            <?php
            if (!empty($shortoutput)) {
                echo '<hr class="styled" /><div id="inpageloop" class="">' . $shortoutput . '</div>';
            }
            ?>

            <?php
            if (comments_open() || get_comments_number()) {
                comments_template('', true);
            }
            ?> 
        </div>
    </div>
<?php endwhile; ?>
<div id="widgetarea-one" class="col-md-4 bd-left-minus">
    <h1 class="title compensate-bs" style="margin-bottom: 25px">Sezioni</h1>
<?php get_template_part('sidebar', 'archive'); ?>     
</div>    
<?php get_footer(); ?>

