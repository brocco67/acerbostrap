<?php

/**
 * Initialize the custom theme options.
 */
add_action('admin_init', 'custom_theme_options');

/**
 * Build the custom settings & update OptionTree.
 */
function custom_theme_options() {

    /* OptionTree is not loaded yet */
    if (!function_exists('ot_settings_id'))
        return false;

    /**
     * Get a copy of the saved settings array. 
     */
    $saved_settings = get_option(ot_settings_id(), array());

    /**
     * Custom settings array that will eventually be 
     * passes to the OptionTree Settings API Class.
     */
    $custom_settings = array(
        'contextual_help' => array(
            'sidebar' => ''
        ),
        'sections' => array(
            array(
                'id' => 'generali',
                'title' => 'Impostazioni generali'
            ),
            array(
                'id' => 'set_avvisi',
                'title' => 'Avvisi'
            ),
	        array(
		        'id' => 'set_hp',
		        'title' => 'Homepage'
	        ),
            array(
                'id' => 'sett_swipe_slider',
                'title' => 'Slider di apertura'
            ),
            array(
                'id' => 'set_content',
                'title' => 'In primo piano'
            ),
            array(
                'id' => 'set_featured',
                'title' => 'In evidenza'
            ),
            array(
                'id' => 'set_fastlink',
                'title' => 'Acerbo appuntamenti'
            ),
            array(
                'id' => 'set_certificazioni',
                'title' => 'Certificazioni'
            ),
            array(
                'id' => 'set_partner',
                'title' => 'Loghi partner'
            ),
            array(
                'id' => 'acerbo_social',
                'title' => 'Social'
            )
        ),
        'settings' => array(
            array(
                'id' => 'site_title',
                'label' => 'Impostazioni per il titolo e sottotitolo del sito',
                'desc' => 'Impostazioni per il titolo del sito e il sottotitolo. Non richiede l\'inserimento di immagini.',
                'std' => '',
                'type' => 'list-item',
                'section' => 'generali',
                'rows' => '',
                'post_type' => '',
                'taxonomy' => '',
                'min_max_step' => '',
                'class' => '',
                'condition' => '',
                'operator' => 'and'
            ),
	        array(
		        'id' => 'hp_text',
		        'label' => 'Testo per la homepage',
		        'desc' => 'Il testo da includere nella Homepage',
		        'std' => '',
		        'type' => 'textarea',
		        'section' => 'set_hp',
		        'rows' => '20',
		        'post_type' => '',
		        'taxonomy' => '',
		        'min_max_step' => '',
		        'class' => ''
	        ),
            array(
                'id' => 'avviso_show',
                'label' => 'Mostro avviso?',
                'desc' => 'Se sì, l\'avviso impostato nell\'area qui in basso verrà visualizzato su tutto il sito. L\'utente può chiudere l\'avviso con un clic sull\'icona a forma di x presente in alto a destra dell\'avviso stesso.',
                'std' => '',
                'type' => 'radio',
                'section' => 'set_avvisi',
                'rows' => '',
                'post_type' => '',
                'taxonomy' => '',
                'min_max_step' => '',
                'class' => '',
                'condition' => '',
                'operator' => 'and',
                'choices' => array(
                    array(
                        'value' => 'yes',
                        'label' => 'Sì',
                        'src' => ''
                    ),
                    array(
                        'value' => 'no',
                        'label' => 'No',
                        'src' => ''
                    )
                )
            ),
            array(
                'id' => 'avviso_text',
                'label' => 'Testo dell\'avviso',
                'desc' => 'Il testo da includere nell\'avviso: è bene far precedere l\'avviso con un\'etichetta in grassetto (Avviso importante, Attenzione ecc.): esempio <code><strong>Avviso importante!</strong> Questa striscia viene mostrata su tutto il sito quando ci sono avvisi importanti. Può essere modificata o disabilitata dalle opzioni del tema.</code>',
                'std' => '',
                'type' => 'textarea',
                'section' => 'set_avvisi',
                'rows' => '',
                'post_type' => '',
                'taxonomy' => '',
                'min_max_step' => '',
                'class' => '',
                'condition' => 'avviso_show:is(yes)',
                'operator' => 'and'
            ),
            array(
                'id' => 'avviso_type',
                'label' => 'Tipologia dell\'avviso',
                'desc' => 'La tipologia dell\'avviso su cui sarà scelto anche il colore: <span style="color:#3c763d">Successo</span>, <span style="color:#8a6d3b">Attenzione</span>, <span style="color:#a94442">Pericolo</span>, <span style="color:#d9edf7">Informazioni</span>',
                'std' => 'warning',
                'type' => 'radio',
                'section' => 'set_avvisi',
                'rows' => '',
                'post_type' => '',
                'taxonomy' => '',
                'min_max_step' => '',
                'class' => '',
                'condition' => 'avviso_show:is(yes)',
                'operator' => 'and',
                'choices' => array(
                    array(
                        'value' => 'success',
                        'label' => 'Successo',
                        'src' => ''
                    ),
                    array(
                        'value' => 'warning',
                        'label' => 'Attenzione',
                        'src' => ''
                    ),
                    array(
                        'value' => 'danger',
                        'label' => 'Pericolo',
                        'src' => ''
                    ),
                    array(
                        'value' => 'info',
                        'label' => 'Informazioni',
                        'src' => ''
                    )
                )
            ),
            array(
                'id' => 'slider_show',
                'label' => 'Mostro lo slider?',
                'desc' => 'Mostro lo slider di immagini in homepage?',
                'std' => 'no',
                'type' => 'radio',
                'section' => 'sett_swipe_slider',
                'rows' => '',
                'post_type' => '',
                'taxonomy' => '',
                'min_max_step' => '',
                'class' => '',
                'condition' => '',
                'operator' => 'and',
                'choices' => array(
                    array(
                        'value' => 'yes',
                        'label' => 'Sì',
                        'src' => ''
                    ),
                    array(
                        'value' => 'no',
                        'label' => 'No',
                        'src' => ''
                    )
                )
            ),
            array(
                'id' => 'swipe_slider',
                'label' => 'Slider di immagini in homepage',
                'desc' => 'Lo slider di immagini in homepage',
                'std' => 'null',
                'type' => 'list-item',
                'section' => 'sett_swipe_slider',
                'rows' => '',
                'post_type' => '',
                'taxonomy' => '',
                'min_max_step' => '',
                'class' => '',
                'condition' => '',
                'operator' => 'and'
            ),
            array(
                'id' => 'hp_type',
                'label' => 'In primo piano',
                'desc' => 'Scegliere il tipo di contenuto da usare come box di apertura del sito',
                'std' => '',
                'type' => 'radio',
                'section' => 'set_content',
                'rows' => '',
                'post_type' => '',
                'taxonomy' => '',
                'min_max_step' => '',
                'class' => '',
                'condition' => '',
                'operator' => 'and',
                'choices' => array(
                    array(
                        'value' => 'youtube',
                        'label' => 'Video di Youtube',
                        'src' => ''
                    ),
                    array(
                        'value' => 'zaccordion',
                        'label' => 'Slider di articoli',
                        'src' => ''
                    ),
                    array(
                        'value' => 'set_hp_html_code',
                        'label' => 'Codice HTML',
                        'src' => ''
                    ),
                    array(
                        'value' => 'null',
                        'label' => 'Vuoto',
                        'src' => ''
                    )
                )
            ),
            array(
                'id' => 'hp_post_01',
                'label' => 'Contenuto numero 1',
                'desc' => 'Seleziona il contenuto da visualizzare in prima posizione',
                'std' => '',
                'type' => 'custom-post-type-select',
                'section' => 'set_content',
                'rows' => '',
                'post_type' => 'post,circolare,page',
                'taxonomy' => '',
                'min_max_step' => '',
                'class' => '',
                'condition' => 'hp_type:is(zaccordion)',
                'operator' => 'and'
            ),
            array(
                'id' => 'hp_post_02',
                'label' => 'Contenuto numero 2',
                'desc' => 'Seleziona il contenuto da visualizzare in seconda posizione',
                'std' => '',
                'type' => 'custom-post-type-select',
                'section' => 'set_content',
                'rows' => '',
                'post_type' => 'post,circolare,page',
                'taxonomy' => '',
                'min_max_step' => '',
                'class' => '',
                'condition' => 'hp_type:is(zaccordion)',
                'operator' => 'and'
            ),
            array(
                'id' => 'hp_post_03',
                'label' => 'Contenuto numero 3',
                'desc' => 'Seleziona il contenuto da visualizzare in terza posizione',
                'std' => '',
                'type' => 'custom-post-type-select',
                'section' => 'set_content',
                'rows' => '',
                'post_type' => 'post,circolare,page',
                'taxonomy' => '',
                'min_max_step' => '',
                'class' => '',
                'condition' => 'hp_type:is(zaccordion)',
                'operator' => 'and'
            ),
            array(
                'id' => 'hp_post_04',
                'label' => 'Contenuto numero 4',
                'desc' => 'Seleziona il contenuto da visualizzare in quarta posizione',
                'std' => '',
                'type' => 'custom-post-type-select',
                'section' => 'set_content',
                'rows' => '',
                'post_type' => 'post,circolare,page',
                'taxonomy' => '',
                'min_max_step' => '',
                'class' => '',
                'condition' => 'hp_type:is(zaccordion)',
                'operator' => 'and'
            ),
            array(
                'id' => 'hp_post_05',
                'label' => 'Contenuto numero 5',
                'desc' => 'Seleziona il contenuto da visualizzare in quinta posizione',
                'std' => '',
                'type' => 'custom-post-type-select',
                'section' => 'set_content',
                'rows' => '',
                'post_type' => 'post,circolare,page',
                'taxonomy' => '',
                'min_max_step' => '',
                'class' => '',
                'condition' => 'hp_type:is(zaccordion)',
                'operator' => 'and'
            ),
            array(
                'id' => 'hp_post_06',
                'label' => 'Contenuto numero 6',
                'desc' => 'Seleziona il contenuto da visualizzare in sesta posizione',
                'std' => '',
                'type' => 'custom-post-type-select',
                'section' => 'set_content',
                'rows' => '',
                'post_type' => 'post,circolare,page',
                'taxonomy' => '',
                'min_max_step' => '',
                'class' => '',
                'condition' => 'hp_type:is(zaccordion)',
                'operator' => 'and'
            ),
            array(
                'id' => 'yt_iframe',
                'label' => 'Youtube Video',
                'desc' => 'L\'ID del video di Youtube da includere nella homepage. Attenzione: solo l\'ID, non l\'intero codice.',
                'std' => '',
                'type' => 'text',
                'section' => 'set_content',
                'rows' => '',
                'post_type' => '',
                'taxonomy' => '',
                'min_max_step' => '',
                'class' => '',
                'condition' => 'hp_type:is(youtube)',
                'operator' => 'and'
            ),
            array(
                'id' => 'open_content_title',
                'label' => 'Titolo',
                'desc' => 'Il titolo del contenuto di apertura',
                'std' => '',
                'type' => 'text',
                'section' => 'set_content',
                'rows' => '',
                'post_type' => '',
                'taxonomy' => '',
                'min_max_step' => '',
                'class' => '',
                'condition' => 'hp_type:is(set_hp_html_code), hp_type:is(youtube)',
                'operator' => 'and'
            ),
            array(
                'id' => 'hp_html_code',
                'label' => 'Codice HTML',
                'desc' => '',
                'std' => '',
                'type' => 'textarea-simple',
                'section' => 'set_content',
                'rows' => '',
                'post_type' => '',
                'taxonomy' => '',
                'min_max_step' => '',
                'class' => '',
                'condition' => 'hp_type:is(set_hp_html_code)',
                'operator' => 'and'
            ),
            array(
                'id' => 'in_evidenza_show',
                'label' => 'Mostro i box in evidenza veloci?',
                'desc' => 'Mostro li box con i collegamenti in evidenza in homepage?',
                'std' => 'no',
                'type' => 'radio',
                'section' => 'set_featured',
                'rows' => '',
                'post_type' => '',
                'taxonomy' => '',
                'min_max_step' => '',
                'class' => '',
                'condition' => '',
                'operator' => 'and',
                'choices' => array(
                    array(
                        'value' => 'yes',
                        'label' => 'Sì',
                        'src' => ''
                    ),
                    array(
                        'value' => 'no',
                        'label' => 'No',
                        'src' => ''
                    )
                )
            ),
            array(
                'id' => 'in_evidenza',
                'label' => 'In evidenza',
                'desc' => 'Popola la sezione In Evidenza della homepage. Se viene inclusa un\'immagine non sarà visualizzato il testo. L\'immagine da aggiungere deve essere maggiore di 262x147 pixel',
                'std' => '',
                'type' => 'list-item',
                'section' => 'set_featured',
                'rows' => '',
                'post_type' => '',
                'taxonomy' => '',
                'min_max_step' => '',
                'class' => '',
                'condition' => 'in_evidenza_show:is(yes)',
                'operator' => 'and'
            ),
            array(
                'id' => 'fastlink_show',
                'label' => 'Mostro gli appuntamenti dell\'Acerbo?',
                'desc' => 'Mostro il box con l\'elenco degli ultimi appuntamenti? Nel caso si scelga "Dinamico" il box verrà popolato con gli ultimi aggiornamenti pubblicati nella sezione "Appuntamenti"',
                'std' => 'no',
                'type' => 'radio',
                'section' => 'set_fastlink',
                'rows' => '',
                'post_type' => '',
                'taxonomy' => '',
                'min_max_step' => '',
                'class' => '',
                'condition' => '',
                'operator' => 'and',
                'choices' => array(
                    array(
                        'value' => 'yes',
                        'label' => 'Sì',
                        'src' => ''
                    ),
                    array(
                        'value' => 'dynamic',
                        'label' => 'Sì, dinamico',
                        'src' => ''
                    ),
                    array(
                        'value' => 'no',
                        'label' => 'No',
                        'src' => ''
                    )
                )
            ),
            array(
                'id' => 'fastlink_text',
                'label' => 'Testo introduttivo',
                'desc' => 'Il testo da visualizzare prima degli appuntamenti veloci. Se vuoto, non viene visualizzato nulla.',
                'std' => '',
                'type' => 'textarea',
                'section' => 'set_fastlink',
                'rows' => '3',
                'post_type' => '',
                'taxonomy' => '',
                'min_max_step' => '',
                'class' => '',
                'condition' => 'fastlink_show:is(yes) OR fastlink_show:is(dynamic)',
                'operator' => 'and'
            ),
            array(
                'id' => 'fastlink',
                'label' => 'Collegamenti veloci',
                'desc' => 'Popola la sezione Appuntamenti in Homepage. L\'immagine non viene visualizzata, ma viene visualizzato solo il link e il titolo',
                'std' => '',
                'type' => 'list-item',
                'section' => 'set_fastlink',
                'rows' => '',
                'post_type' => '',
                'taxonomy' => '',
                'min_max_step' => '',
                'class' => '',
                'condition' => 'fastlink_show:is(yes)',
                'operator' => 'and'
            ),
            array(
                'id' => 'certificazioni_show',
                'label' => 'Mostro le certificazioni?',
                'desc' => 'Mostro lo slider di certificazioni in homepage?',
                'std' => 'no',
                'type' => 'radio',
                'section' => 'set_certificazioni',
                'rows' => '',
                'post_type' => '',
                'taxonomy' => '',
                'min_max_step' => '',
                'class' => '',
                'condition' => '',
                'operator' => 'and',
                'choices' => array(
                    array(
                        'value' => 'yes',
                        'label' => 'Sì',
                        'src' => ''
                    ),
                    array(
                        'value' => 'no',
                        'label' => 'No',
                        'src' => ''
                    )
                )
            ),
            array(
                'id' => 'certificazioni_text',
                'label' => 'Testo introduttivo',
                'desc' => 'Il testo da visualizzare prima dello slider delle certificazioni.',
                'std' => '',
                'type' => 'textarea',
                'section' => 'set_certificazioni',
                'rows' => '3',
                'post_type' => '',
                'taxonomy' => '',
                'min_max_step' => '',
                'class' => '',
                'condition' => 'certificazioni_show:is(yes)',
                'operator' => 'and'
            ),
            array(
                'id' => 'certificazioni',
                'label' => 'Certificazioni',
                'desc' => 'Popola la sezione Certificazioni in Homepage. L\'immagine da aggiungere deve essere maggiore di 150x90 pixel.',
                'std' => '',
                'type' => 'list-item',
                'section' => 'set_certificazioni',
                'rows' => '',
                'post_type' => '',
                'taxonomy' => '',
                'min_max_step' => '',
                'class' => '',
                'condition' => 'certificazioni_show:is(yes)',
                'operator' => 'and'
            ),
            array(
                'id' => 'partner_show',
                'label' => 'Mostro i partner?',
                'desc' => 'Mostro lo slider con i loghi dei partner?',
                'std' => 'no',
                'type' => 'radio',
                'section' => 'set_partner',
                'rows' => '',
                'post_type' => '',
                'taxonomy' => '',
                'min_max_step' => '',
                'class' => '',
                'condition' => '',
                'operator' => 'and',
                'choices' => array(
                    array(
                        'value' => 'yes',
                        'label' => 'Sì',
                        'src' => ''
                    ),
                    array(
                        'value' => 'no',
                        'label' => 'No',
                        'src' => ''
                    )
                )
            ),
            array(
                'id' => 'partner_text',
                'label' => 'Testo introduttivo',
                'desc' => 'Il testo da visualizzare prima dello slider dei loghi dei partner.',
                'std' => '',
                'type' => 'textarea',
                'section' => 'set_partner',
                'rows' => '3',
                'post_type' => '',
                'taxonomy' => '',
                'min_max_step' => '',
                'class' => '',
                'condition' => 'partner_show:is(yes)',
                'operator' => 'and'
            ),
            array(
                'id' => 'partner',
                'label' => 'I partner dell\'Acerbo',
                'desc' => 'Popola lo slider dei loghi in Homepage. Titolo e descrizione non vengono valorizzati nel front-end. L\'immagine da aggiungere deve essere maggiore di 400x100 pixel (HD) o 200x50 (SD).',
                'std' => '',
                'type' => 'list-item',
                'section' => 'set_partner',
                'rows' => '',
                'post_type' => '',
                'taxonomy' => '',
                'min_max_step' => '',
                'class' => '',
                'condition' => 'partner_show:is(yes)',
                'operator' => 'and'
            ),
            array(
                'id' => 'acerbo_facebook',
                'label' => 'Facebook (ID della pagina)',
                'desc' => 'Id, non l\'intero indirizzo, della pagina Facebook della scuola.',
                'std' => '311961392258',
                'type' => 'text',
                'section' => 'acerbo_social',
                'rows' => '',
                'post_type' => '',
                'taxonomy' => '',
                'min_max_step' => '',
                'class' => '',
                'condition' => '',
                'operator' => 'and'
            ),
            array(
                'id' => 'acerbo_twitter',
                'label' => 'Twitter (nome utente)',
                'desc' => 'Il nome utente del profilo Facebook (ad esempio: acerbosocial)',
                'std' => 'acerbosocial',
                'type' => 'text',
                'section' => 'acerbo_social',
                'rows' => '',
                'post_type' => '',
                'taxonomy' => '',
                'min_max_step' => '',
                'class' => '',
                'condition' => '',
                'operator' => 'and'
            ),
            array(
                'id' => 'acerbo_youtube',
                'label' => 'Youtube (nome utente)',
                'desc' => 'Il nome utente dell\'utente Youtube (ad esempio: acerbochannel).',
                'std' => 'acerbochannel',
                'type' => 'text',
                'section' => 'acerbo_social',
                'rows' => '',
                'post_type' => '',
                'taxonomy' => '',
                'min_max_step' => '',
                'class' => '',
                'condition' => '',
                'operator' => 'and'
            ),
	        array(
		        'id' => 'acerbo_instagram',
		        'label' => 'Instagram (nome utente)',
		        'desc' => 'Il nome dell\'account Instagram (ad esempio: acerbosocialpescara).',
		        'std' => 'acerbosocialpescara',
		        'type' => 'text',
		        'section' => 'acerbo_social',
		        'rows' => '',
		        'post_type' => '',
		        'taxonomy' => '',
		        'min_max_step' => '',
		        'class' => '',
		        'condition' => '',
		        'operator' => 'and'
	        )
        )
    );

    /* allow settings to be filtered before saving */
    $custom_settings = apply_filters(ot_settings_id() . '_args', $custom_settings);

    /* settings are not the same update the DB */
    if ($saved_settings !== $custom_settings) {
        update_option(ot_settings_id(), $custom_settings);
    }

    /* Lets OptionTree know the UI Builder is being overridden */
    global $ot_has_custom_theme_options;
    $ot_has_custom_theme_options = true;
}
