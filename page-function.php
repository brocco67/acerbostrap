<?php

/*
  Template Name: Pagina di funzione
 */

include 'vendor/autoload.php';

//$files = glob('D:/tmp/AS_2013-2014/*.{docx,doc,pdf}', GLOB_BRACE);
$files = glob('/web/htdocs/www.istitutotecnicoacerbope.edu.it/home/circolari/AS_2008-2009/*.{zip,docx,doc,pdf,xls}', GLOB_BRACE);
foreach ($files as $key => $file) {

    $file_parts = pathinfo($file);

    echo '<pre>';
    echo $key . ' - ' . $file;

    if ($file_parts['extension'] == 'pdf') {
        try {
            // Parse pdf file and build necessary objects.
            $parser = new \Smalot\PdfParser\Parser();
            $pdf = $parser->parseFile($file);

            $text = $pdf->getText();
            $details = $pdf->getDetails();

            $filetime = isset($details['ModDate']) ? $details['ModDate'] : null;

            $kea = new KEA();
            $kea->set_langID('it');
            $tags = $kea->Autotag($text);
            $content = implode(", ", array_keys($tags));
        } catch (Exception $e) {
            echo 'Caught exception: ', $e->getMessage(), "\n";
        }
    }

    if ($file_parts['extension'] == 'docx') {
        try {
            $docObj = new DocxConversion($file);
            $text = $docObj->convertToText();

            $kea = new KEA();
            $kea->set_langID('it');
            $tags = $kea->Autotag($text);
            $content = implode(", ", array_keys($tags));
        } catch (Exception $e) {
            echo 'Caught exception: ', $e->getMessage(), "\n";
        }
    }

//    echo $content;
//    echo '</pre>';

    $content = empty($content) ? 'File Acerbo' : $content;
    $filetime = empty($filetime) ? null : $filetime;

    $id = import_file($file, 4, 'file', $content, $filetime);

    if (is_wp_error($id)) {
        echo '<br /><span style="color:crimson">' . sprintf(__('<em>%s</em> was <strong>not</strong> imported due to an error: %s', 'add-from-server'), esc_html($file), $id->get_error_message()).'</span>';
    } else {
        //increment the gallery count
        unlink($file);
        echo '<br />' . sprintf(__('<em>%s</em> has been added to Media library', 'add-from-server'), esc_html($file));
        
    }
    echo '</pre>';
    flush();
    wp_ob_end_flush_all();
}

function import_file($file, $post_id = 0, $import_date = 'file', $content, $filetime) {
    set_time_limit(120);

    // Initially, Base it on the -current- time.
    $time = current_time('mysql', 1);
    // Next, If it's post to base the upload off:
    if ('post' == $import_date && $post_id > 0) {
        $post = get_post($post_id);
        if ($post && substr($post->post_date_gmt, 0, 4) > 0)
            $time = $post->post_date_gmt;
    } elseif ('file' == $import_date) {

        if (!empty($filetime)) {
            $time = date_format(date_create($filetime), 'Y-m-d H:i:s');
        } else {
            $time = gmdate('Y-m-d H:i:s', @filemtime($file));
        }
    }

    //return;
    // A writable uploads dir will pass this test. Again, there's no point overriding this one.
    if (!( ( $uploads = wp_upload_dir($time) ) && false === $uploads['error'] ))
        return new WP_Error('upload_error', $uploads['error']);

    $wp_filetype = wp_check_filetype($file, null);

    extract($wp_filetype);

    if ((!$type || !$ext ) && !current_user_can('unfiltered_upload'))
        return new WP_Error('wrong_file_type', __('Sorry, this file type is not permitted for security reasons.')); //A WP-core string..



        
//Is the file already in the uploads folder?

    $tmp = str_replace('\\', '/', $uploads['path']) . '/' . basename($file);

    if (file_exists($tmp) && is_file($tmp)) {
        unlink($file);
        return new WP_Error('file_exists', __('Sorry, That file already exists in the WordPress media library.', 'add-from-server'));
    } else {
        $filename = wp_unique_filename($uploads['path'], basename($file));

        // copy the file to the uploads dir
        $new_file = $uploads['path'] . '/' . $filename;
        if (false === @copy($file, $new_file))
            return new WP_Error('upload_error', sprintf(__('The selected file could not be copied to %s.', 'add-from-server'), $uploads['path']));

        // Set correct file permissions
        $stat = stat(dirname($new_file));
        $perms = $stat['mode'] & 0000666;
        @ chmod($new_file, $perms);
        // Compute the URL
        $url = $uploads['url'] . '/' . $filename;

        if ('file' == $import_date) {
            if (!empty($filetime)) {
                $time = date_format(date_create($filetime), 'Y-m-d H:i:s');
            } else {
                $time = gmdate('Y-m-d H:i:s', @filemtime($file));
            }
        }
    }

    //Apply upload filters
    $return = apply_filters('wp_handle_upload', array('file' => $new_file, 'url' => $url, 'type' => $type));
    $new_file = $return['file'];
    $url = $return['url'];
    $type = $return['type'];

    $title = preg_replace('/\.[^.]+$/', '', basename($filename));
    $title = preg_replace('/-_-/', '/', basename($title));
    $title = ucwords(strtolower($title));

    $content = empty($content) ? 'file' : $content;

    if ($time) {
        $post_date_gmt = $time;
        $post_date = $time;
    } else {
        $post_date = current_time('mysql');
        $post_date_gmt = current_time('mysql', 1);
    }

    // Construct the attachment array
    $attachment = array(
        'post_mime_type' => $type,
        'guid' => $url,
        'post_parent' => $post_id,
        'post_title' => $title,
        'post_name' => $title,
        'post_content' => $content,
        'post_date' => $post_date,
        'post_date_gmt' => $post_date_gmt
    );



    $attachment = apply_filters('afs-import_details', $attachment, $file, $post_id, $import_date);

    //Win32 fix:
    $new_file = str_replace(strtolower(str_replace('\\', '/', $uploads['basedir'])), $uploads['basedir'], $new_file);

    // Save the data
    $id = wp_insert_attachment($attachment, $new_file, $post_id);

    //update_post_meta( $id, '_wp_attached_file', $uploads['subdir'] . '/' . $filename );

    return $id;
}
