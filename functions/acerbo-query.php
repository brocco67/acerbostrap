<?php

/**
 * Includi i post type nel loop
 */
function acerbo_get_post( $query ) {
	if ( is_admin() ) {
		return $query;
	}
	/**
	 * Includi i post type nella ricerca
	 */
	if ( $query->is_search ) {
		$query->set( 'post_type', array( 'post', 'circolare', 'avviso', 'appuntamento' ) );
	}
	/**
	 * Includi i post type nel loop
	 */
	if ( ( is_home() || is_archive() ) && $query->is_main_query() ) {
		if ( ! is_post_type_archive() ) {
			$query->set( 'post_type', array( 'post', 'circolare', 'avviso', 'appuntamento' ) );
		}
	}


	/**
	 * Includi i post type nel feed
	 */
	if ( is_feed() && $query->is_main_query() ) {
		$query->set( 'post_type', array( 'post', 'circolare', 'avviso', 'appuntamento' ) );
	}


	/**
	 * Includi i post type Contributo nella pagina autore
	 */
	if ( $query->is_author ) {
		$query->set( 'post_type', array( 'contributo' ) );
	}

	return $query;
}

add_action('pre_get_posts', 'acerbo_get_post');
