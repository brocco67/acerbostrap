<?php


function acerbo_add_project_management_role() {
	add_role( 'docente',
		'Docente',
		array(
			'read'          => true,
			'edit_posts'    => true,
			'delete_posts'  => true,
			'publish_posts' => true,
			'upload_files'  => true,
		)
	);
}

add_action( 'init', 'acerbo_add_project_management_role' );


add_action( 'admin_init', 'acerbo_add_role_caps', 999 );
function acerbo_add_role_caps() {

	// Add the roles you'd like to administer the custom post types
	$roles_admins = array( 'editor', 'administrator' );

	$roles_users = array( 'docente' );

	// Loop through each role and assign capabilities
	foreach ( $roles_admins as $roles_admin ) {

		$role = get_role( $roles_admin );

		$role->add_cap( 'read' );
		$role->add_cap( 'read_contributo' );
		$role->add_cap( 'read_private_contributi' );
		$role->add_cap( 'edit_contributo' );
		$role->add_cap( 'edit_contributi' );
		$role->add_cap( 'edit_others_contributi' );
		$role->add_cap( 'edit_published_contributi' );
		$role->add_cap( 'publish_contributi' );
		$role->add_cap( 'delete_others_contributi' );
		$role->add_cap( 'delete_private_contributi' );
		$role->add_cap( 'delete_published_contributi' );

	}

	// Loop through each role and assign capabilities
	foreach ( $roles_users as $roles_user ) {

		$role = get_role( $roles_user );

		$role->add_cap( 'read' );
		$role->add_cap( 'read_contributo' );
		$role->add_cap( 'upload_files' );
		$role->remove_cap( 'read_private_contributi' );
		$role->add_cap( 'edit_contributo' );
		$role->add_cap( 'edit_contributi' );
		$role->remove_cap( 'edit_others_contributi' );
		$role->add_cap( 'edit_published_contributi' );
		$role->add_cap( 'publish_contributi' );
		$role->remove_cap( 'delete_others_contributi' );
		$role->remove_cap( 'delete_private_contributi' );
		$role->add_cap( 'delete_published_contributi' );

	}
}


/**
 * POST TYPE
 */
add_action( 'init', 'acerbo_create_post_type' );


function acerbo_create_post_type() {

	register_post_type( 'contributo', array(
			'labels'          => array(
				'name'          => __( 'Contributi docenti' ),
				'singular_name' => __( 'Contributo' ),
				'add_new'       => __( 'Nuovo contributo' ),
				'add_new_item'  => __( 'Aggiungi nuovo contributo' ),
			),
			'public'          => true,
			'has_archive'     => true,
			'menu_position'   => 5,
			'supports'        => array( 'title', 'editor', 'excerpt', 'revisions', 'thumbnail' ),
			'taxonomies'      => array( 'tag_docenti' ),
			'capability_type' => array( 'contributo', 'contributi' ),
			'map_meta_cap'    => true,
		)
	);

	register_post_type( 'foto_e_video', array(
			'labels'        => array(
				'name'          => __( 'Foto e video' ),
				'singular_name' => __( 'Foto e video' ),
				'add_new'       => __( 'Nuova Foto o video' ),
				'add_new_item'  => __( 'Aggiungi nuova Foto o video' ),
			),
			'public'        => true,
			'has_archive'   => true,
			'menu_position' => 5,
			'supports'      => array( 'title', 'editor', 'excerpt', 'revisions', 'thumbnail' ),
			'rewrite'       => array( 'slug' => 'foto_e_video' )
		)
	);


	register_post_type( 'circolare', array(
			'labels'        => array(
				'name'          => __( 'Circolari' ),
				'singular_name' => __( 'Circolare' ),
				'add_new'       => __( 'Nuova circolare' ),
				'add_new_item'  => __( 'Aggiungi nuova circolare' ),
			),
			'public'        => true,
			'has_archive'   => true,
			'menu_position' => 5,
			'supports'      => array( 'title', 'editor', 'excerpt', 'revisions', 'thumbnail' ),
			'taxonomies'    => array( 'post_tag', 'category', 'tema' )
		)
	);

	register_post_type( 'appuntamento', array(
			'labels'        => array(
				'name'          => __( 'Appuntamenti' ),
				'singular_name' => __( 'Appuntamento' ),
				'add_new'       => __( 'Nuovo appuntamento' ),
				'add_new_item'  => __( 'Aggiungi nuovo appuntamento' ),
			),
			'public'        => true,
			'has_archive'   => true,
			'menu_position' => 7,
			'supports'      => array( 'title', 'editor', 'excerpt', 'revisions', 'thumbnail' ),
			'taxonomies'    => array( 'post_tag', 'category', 'tema' )
		)
	);

	register_post_type( 'avviso', array(
			'labels'        => array(
				'name'          => __( 'Avvisi' ),
				'singular_name' => __( 'Avviso' ),
				'add_new'       => __( 'Nuovo avviso' ),
				'add_new_item'  => __( 'Aggiungi nuovo avviso' ),
			),
			'public'        => true,
			'has_archive'   => true,
			'menu_position' => 6,
			'supports'      => array( 'title', 'editor', 'excerpt', 'revisions', 'thumbnail' ),
			'taxonomies'    => array( 'post_tag', 'category', 'tema' )
		)
	);
}


/**
 * TASSONOMIE
 */
add_action( 'init', 'create_acerbo_taxonomies', 0 );

function create_acerbo_taxonomies() {
	// Add new taxonomy, make it hierarchical (like categories)
	$labels = array(
		'name'              => _x( 'Tema', 'taxonomy general name' ),
		'singular_name'     => _x( 'Tema', 'taxonomy singular name' ),
		'search_items'      => __( 'Cerca temi' ),
		'all_items'         => __( 'Tutti temi' ),
		'parent_item'       => __( 'Tema padre' ),
		'parent_item_colon' => __( 'Tema padre:' ),
		'edit_item'         => __( 'Modifica tema' ),
		'update_item'       => __( 'Aggiorna tema' ),
		'add_new_item'      => __( 'Aggiungi nuovo tema' ),
		'new_item_name'     => __( 'Nuovo tema' ),
		'menu_name'         => __( 'Temi' ),
	);

	$args = array(
		'hierarchical'      => true,
		'labels'            => $labels,
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => true,
		'public'            => true,
		'rewrite'           => array( 'slug' => 'tema' ),
	);

	register_taxonomy( 'tema', array( 'page', 'post', 'circolare', 'avviso', 'appuntamento' ), $args );

	$labels = array(
		'name'              => _x( 'Argomento', 'taxonomy general name' ),
		'singular_name'     => _x( 'Argomento', 'taxonomy singular name' ),
		'search_items'      => __( 'Cerca Argomenti' ),
		'all_items'         => __( 'Tutti gli argomenti' ),
		'parent_item'       => __( 'Argomento padre' ),
		'parent_item_colon' => __( 'Argomento padre:' ),
		'edit_item'         => __( 'Modifica argomento' ),
		'update_item'       => __( 'Aggiorna argomento' ),
		'add_new_item'      => __( 'Aggiungi nuovo argomento' ),
		'new_item_name'     => __( 'Nuovo argomento' ),
		'menu_name'         => __( 'Argomenti' ),
	);

	$args = array(
		'hierarchical'      => true,
		'labels'            => $labels,
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => true,
		'public'            => true,
		'rewrite'           => array( 'slug' => 'argomento' ),
		'capabilities'      => array(
			'manage_terms' => 'manage_categories',
			'edit_terms'   => 'manage_categories',
			'delete_terms' => 'manage_categories',
			'assign_terms' => 'read'
		)
	);

	register_taxonomy( 'argomento', array( 'contributo' ), $args );
}
