<?php

remove_shortcode( 'gallery' );
add_shortcode( 'gallery', 'acerbo_gallery_shortcode_bootstrap' );

/**
 * The Gallery shortcode.
 *
 * This implements the functionality of the Gallery Shortcode for displaying
 * WordPress images on a post.
 */
function acerbo_gallery_shortcode_bootstrap( $attr ) {
	$post = get_post();

	static $instance = 0;
	$instance ++;

	if ( ! empty( $attr['ids'] ) ) {
		// 'ids' is explicitly ordered, unless you specify otherwise.
		if ( empty( $attr['orderby'] ) ) {
			$attr['orderby'] = 'post__in';
		}
		$attr['include'] = $attr['ids'];
	}

	// Allow plugins/themes to override the default gallery template.
	$output = apply_filters( 'post_gallery', '', $attr );
	if ( $output != '' ) {
		return $output;
	}

	// We're trusting author input, so let's at least make sure it looks like a valid orderby statement
	if ( isset( $attr['orderby'] ) ) {
		$attr['orderby'] = sanitize_sql_orderby( $attr['orderby'] );
		if ( ! $attr['orderby'] ) {
			unset( $attr['orderby'] );
		}
	}

	extract( shortcode_atts( array(
		'order'      => 'ASC',
		'orderby'    => 'menu_order ID',
		'id'         => $post ? $post->ID : 0,
		'itemtag'    => 'dl',
		'icontag'    => 'dt',
		'captiontag' => 'dd',
		'columns'    => 3,
		'size'       => 'thumbnail',
		'include'    => '',
		'exclude'    => '',
		'link'       => ''
	), $attr, 'gallery' ) );

	$id = intval( $id );
	if ( 'RAND' == $order ) {
		$orderby = 'none';
	}

	if ( ! empty( $include ) ) {
		$_attachments = get_posts( array(
			'include'        => $include,
			'post_status'    => 'inherit',
			'post_type'      => 'attachment',
			'post_mime_type' => 'image',
			'order'          => $order,
			'orderby'        => $orderby
		) );

		$attachments = array();
		foreach ( $_attachments as $key => $val ) {
			$attachments[ $val->ID ] = $_attachments[ $key ];
		}
	} elseif ( ! empty( $exclude ) ) {
		$attachments = get_children( array(
			'post_parent'    => $id,
			'exclude'        => $exclude,
			'post_status'    => 'inherit',
			'post_type'      => 'attachment',
			'post_mime_type' => 'image',
			'order'          => $order,
			'orderby'        => $orderby
		) );
	} else {
		$attachments = get_children( array(
			'post_parent'    => $id,
			'post_status'    => 'inherit',
			'post_type'      => 'attachment',
			'post_mime_type' => 'image',
			'order'          => $order,
			'orderby'        => $orderby
		) );
	}

	if ( empty( $attachments ) ) {
		return '';
	}

	if ( is_feed() ) {
		$output = "\n";
		foreach ( $attachments as $att_id => $attachment ) {
			$output .= wp_get_attachment_link( $att_id, $size, true ) . "\n";
		}

		return $output;
	}

	$itemtag    = tag_escape( $itemtag );
	$captiontag = tag_escape( $captiontag );
	$icontag    = tag_escape( $icontag );
	$valid_tags = wp_kses_allowed_html( 'post' );
	if ( ! isset( $valid_tags[ $itemtag ] ) ) {
		$itemtag = 'dl';
	}
	if ( ! isset( $valid_tags[ $captiontag ] ) ) {
		$captiontag = 'dd';
	}
	if ( ! isset( $valid_tags[ $icontag ] ) ) {
		$icontag = 'dt';
	}

	$columns   = intval( $columns );
	$itemwidth = $columns > 0 ? floor( 100 / $columns ) : 100;
	$float     = is_rtl() ? 'right' : 'left';

	$selector = "gallery-{$instance}";

	$gallery_style = $gallery_div = '';

	$size_class  = sanitize_html_class( $size );
	$gallery_div = "<div id='$selector' class='row gallery galleryid-{$id} gallery-size-{$size_class}'>";
	$output      = apply_filters( 'gallery_style', $gallery_style . "\n\t\t" . $gallery_div );

	$i = 1;


	foreach ( $attachments as $id => $attachment ) {
		if ( ! empty( $link ) && 'file' === $link ) {
			$image_output = wp_get_attachment_link( $id, $size, false, false );
		} elseif ( ! empty( $link ) && 'none' === $link ) {
			$image_output = wp_get_attachment_image( $id, $size, false );
		} else {
			$image_output = wp_get_attachment_link( $id, $size, true, false );
		}

		$image_output = preg_replace( '/height="[0-9]+"/', '', $image_output );
		$image_output = preg_replace( '/width="[0-9]+"/', '', $image_output );
		$image_output = str_replace( 'class="', 'class="center-block img-responsive ', $image_output );
		$image_output = str_replace( 'href=', 'title="' . esc_attr($attachment->post_excerpt) . '" href=', $image_output );
		$image_meta   = wp_get_attachment_metadata( $id );

		$orientation = '';
		if ( isset( $image_meta['height'], $image_meta['width'] ) ) {
			$orientation = ( $image_meta['height'] > $image_meta['width'] ) ? 'portrait' : 'landscape';
		}

		$output .= "<div class='gallery-item col-xs-6 col-sm-4'>";
		$output .= "
			<div class='gallery-icon {$orientation}'>
				$image_output
			</div>";
		if ( $captiontag && trim( $attachment->post_excerpt ) ) {
			$output .= "
				<{$captiontag} class='wp-caption-text gallery-caption'>
				" . wptexturize( $attachment->post_excerpt ) . "
				</{$captiontag}>";
		}
		$output .= "</div>";

		if ( 0 == ( $i % 3 ) ) {
			$output .= '<div class="clearfix visible-sm visible-md visible-lg"></div>';
		}
		if ( 0 == ( $i % 2 ) ) {
			$output .= '<div class="clearfix visible-xs"></div>';
		}
		$i ++;
	}

	$output .= "
		</div>\n";

	return $output;
}

/*
 * Rimuove paragrafi vuori (<p></p>) e interruzioni di linea (<br>) dagli shortcode
 */

function acerbo_clean_shortcodes( $content ) {
	$tags = array(
		'<p>['          => '[',
		']</p>'         => ']',
		']<br>'         => ']',
		']<br />'       => ']',
		'<p></p>'       => '',
		'<p>&nbsp;</p>' => '',
		'<p> </p>'      => ''
	);

	$content = strtr( $content, $tags );

	return $content;
}

add_filter( 'the_content', 'acerbo_clean_shortcodes' );

/**
 * Cambia l'indirizzo dei link delle foto delle gallery all'immagine fisica
 */
function acerbo_gallery_atts( $out, $pairs, $atts ) {

	$atts = shortcode_atts( array(
		'columns' => '3',
		'size'    => 'medium',
	), $atts );

	$out['columns'] = $atts['columns'];
	$out['size']    = $atts['size'];
	$out['link']    = 'file';

	return $out;
}

add_filter( 'shortcode_atts_gallery', 'acerbo_gallery_atts', 10, 3 );

/**
 * Shortcode per la visualizzazione di un loop di articoli
 *
 * @param type $atts
 * @param type $content
 *
 * @return type
 */
function acerbo_showloop( $atts ) {

	$a = shortcode_atts( array(
		'term'         => '',
		'showcat'      => 'no',
		'showtax'      => 'no',
		'showingletax' => 'no'
	), $atts );


	if ( ! is_page() ) {
		//return;
	}

	if ( ! is_array( $a['term'] ) ) {
		$terms = array( $a['term'] );
	}

	foreach ( $terms as $term ) {
		if ( ! term_exists( $term ) ) {
			return 'Attenzione: Il termine non esiste';
		}
	}

	if ( $a['showcat'] == 'yes' ) {
		$categories = get_categories();

		ob_start();

		echo '<div id="tab-container" class="tab-container" role="tabpanel"><ul class="nav nav-pills nav-justified" style="width:inherit">';
		echo '<li role="presentation" class="active"><a href="#tutto" aria-controls="tutto" role="tab" data-toggle="tab">Tutti</a></li>';
		foreach ( $categories as $category ) {
			echo '<li role="presentation"><a href="#' . $category->slug . '" aria-controls="' . $category->name . '" role="tab" data-toggle="tab">' . $category->name . '</a></li>';
		}
		echo '</ul>';
		echo '<div class="tab-content">';
		echo '<div id="tutto" role="tabpanel" class="tab-pane fade in active data-scroll-append">';
		$query = new WP_Query( array(
			'numberposts' => 10,
			'post_type'   => array( 'post', 'circolare', 'avviso' ),
			'tax_query'   => array(
				array(
					'taxonomy' => 'tema',
					'field'    => 'slug',
					'terms'    => $term
				)
			)
		) );
		if ( $query->have_posts() ) : while ( $query->have_posts() ) : $query->the_post();
			get_template_part( 'acerbo', 'loop' );
		endwhile;
			wp_reset_postdata();
		endif;
		echo '<nav><ul class="pager"><li><a href="/aggiornamenti/page/2" data-tmpl="loop" class="data-scroll" data-pt="circolare,avviso,post" data-offset="10">Visualizza aggiornamenti precedenti</a></li></ul></nav></div>';

		foreach ( $categories as $category ) {
			echo '<div id="' . $category->slug . '" role="tabpanel" class="data-scroll-append tab-pane fade">';
			$query = new WP_Query( array(
				'numberposts'   => 10,
				'post_type'     => array( 'post', 'circolare', 'avviso' ),
				'category_name' => $category->slug,
				'tax_query'     => array(
					array(
						'taxonomy' => 'tema',
						'field'    => 'slug',
						'terms'    => $term
					)
				)
			) );
			if ( $query->have_posts() ) : while ( $query->have_posts() ) : $query->the_post();
				get_template_part( 'acerbo', 'loop' );
			endwhile;
				wp_reset_postdata();
			endif;
			echo '<nav><ul class="pager"><li><a href="/categoria/docenti-ata/page/2" data-tmpl="loop" class="data-scroll" data-cat="docenti" data-pt="circolare,avviso,post" data-offset="10">Visualizza aggiornamenti precedenti</a></li></ul></nav></div>';
		}

		echo '</div></div>';

		return ob_get_clean();
	} else {
		ob_start();
		$query = new WP_Query( array(
			'numberposts' => 10,
			'post_type'   => array( 'post', 'circolare', 'avviso' ),
			'tax_query'   => array(
				array(
					'taxonomy' => 'tema',
					'field'    => 'slug',
					'terms'    => $term
				)
			)
		) );
		if ( $query->have_posts() ) : while ( $query->have_posts() ) : $query->the_post();
			get_template_part( 'acerbo', 'loop' );
		endwhile;
			wp_reset_postdata();
		endif;

		return ob_get_clean();
	}
	
}

add_shortcode( 'showloop', 'acerbo_showloop' );


/**
 * Shortcode per l'aggiunta di un link
 *
 * @param type $atts
 * @param type $content
 *
 * @return type
 */
function acerbo_includelink( $atts ) {

	extract( shortcode_atts( array( 'id' => '' ), $atts ) );

	if ( is_numeric( $id ) ) {
		$card_title    = get_the_title( $id );
		$card_url      = get_permalink( $id );
		$card_thumb_id = get_post_thumbnail_id( $id );
		if ( $card_thumb_id ) {
			$card_thumb_src = wp_get_attachment_image_src( $card_thumb_id, 'full' );
		} else {
			$card_thumb_src = wp_get_attachment_image_src( 5929, 'full' );
		}
		$card_excerpt = get_post( $id )->post_excerpt;
	}

	if ( 'publish' === get_post_status( $id ) ) {
		$output = '<div class="media"><div class="media-left"><a href="' . $card_url . '"><img class="media-object" src="' . $card_thumb_src[0] . '?height=75&width=75" alt="immagine" /></a></div><div class="media-body"><h3 class="media-heading"><a href="' . $card_url . '">' . $card_title . '</a></h3>' . $card_excerpt . '</div></div>';
	}

	return $output;
}

add_shortcode( 'includelink', 'acerbo_includelink' );