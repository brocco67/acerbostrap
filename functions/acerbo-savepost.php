<?php

/**
 * Filtri
 */
add_filter('cfs_field_types', 'acerbo_cfs_unixtime');

function acerbo_cfs_unixtime($field_types) {
    $field_types['unixtime'] = trailingslashit(get_template_directory()) . 'inc/cfs/unixtime_fields.php';
    return $field_types;
}

/**
 * Salva i custom field
 * [field_data][5][value] = data dell'avviso
 * [field_data][3][value] = data della circolare
 */
function acerbo_convert_date($params) {

    $id = $params['post_data']['ID'];

    if (isset($params['field_data'][5]['value'])) {
        $data = date("U", strtotime($params['field_data'][5]['value']));
    } elseif (isset($params['field_data'][3]['value'])) {
        $data = date("U", strtotime($params['field_data'][3]['value']));
    } else {
        $data = null;
    }

    if (isset($params['field_data'][7]['value'])) {
        $data_start = date_create_from_format('d/m/Y H:i', $params['field_data'][7]['value']);
        update_post_meta($id, 'evento_unixtime_start', date_format($data_start, 'U'));
    }

    if (isset($params['field_data'][8]['value'])) {
        $data_end = date_create_from_format('d/m/Y H:i', $params['field_data'][8]['value']);
        update_post_meta($id, 'evento_unixtime_end', date_format($data_end, 'U'));
    }

//echo '<pre>';
////echo $datetime_start;
//print_r($params);
//echo date_format($data_start, 'U');
//echo '<br/>';
//echo date_format($data_end, 'U');
//echo '</pre>';
//die();

    if (is_numeric($data) && (int) $data == $data) {
        update_post_meta($id, 'data_orderby', $data);
    }
}

add_action('cfs_after_save_input', 'acerbo_convert_date');

function acerbo_convert_date_original($post_id, $post, $update) {

    if ($post->post_type == 'post') {
        $data = date("U", strtotime($post->post_date));
        update_post_meta($post_id, 'data_orderby', $data);
    }
}

add_action('save_post', 'acerbo_convert_date_original', 10, 3);
