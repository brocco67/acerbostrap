<?php

/**
 * WIDGET
 */
class CustomTaxonomyWidget extends WP_Widget {

    public function __construct() {
        parent::__construct(
                'custom_taxonomy_widget', 'Tassonomie personalizzate', array('description' => 'Crea un widget con le sottocategorie delle tassonomie personalizzate')
        );
    }

    public function widget($args, $instance) {
        extract($args);
        $title = apply_filters('widget_title', $instance['title']);
        $order_options = (isset($instance['order_options'])) ? explode('/', $instance['order_options']) : array('', '');
        $get_terms_args = array(
            'number' => (isset($instance['max_terms'])) ? $instance['max_terms'] : ''
        );
        $terms = get_terms($instance['custom_taxonomies'], $get_terms_args);

        if (empty($terms)) {
            return;
        }
        echo $before_widget;
        if (!empty($title))
            echo $before_title . $title . $after_title;
        ?>
        <ul>
            <?php foreach ($terms as $term): ?>
            <?php if ($term->parent == "0" && $term->count > 0) : ?>
                    <li class="<?php echo ($term->parent != "0") ? 'taxonomy-has-parent' : null; ?>">
                        <a href="<?php echo get_term_link($term); ?>"><?php echo $term->name; ?></a>
                    </li>
                <?php endif; ?>
        <?php endforeach; ?>
        </ul>
        <?php
        echo $after_widget;
    }

    public function form($instance) {
        $field_data = array(
            'title' => array(
                'id' => $this->get_field_id('title'),
                'name' => $this->get_field_name('title'),
                'value' => (isset($instance['title'])) ? $instance['title'] : __('New Title')
            ),
            'taxonomies' => array(
                'name' => $this->get_field_name('custom_taxonomies'),
                'value' => (isset($instance['custom_taxonomies'])) ? $instance['custom_taxonomies'] : ''
            ),
            'max_terms' => array(
                'id' => $this->get_field_id('max_terms'),
                'name' => $this->get_field_name('max_terms'),
                'value' => (isset($instance['max_terms'])) ? $instance['max_terms'] : ''
            ),
            'exclude' => array(
                'id' => $this->get_field_id('exclude'),
                'name' => $this->get_field_name('exclude'),
                'value' => (isset($instance['exclude'])) ? $instance['exclude'] : ''
            ),
            'include' => array(
                'id' => $this->get_field_id('include'),
                'name' => $this->get_field_name('include'),
                'value' => (isset($instance['include'])) ? $instance['include'] : ''
            ),
            'child' => array(
                'id' => $this->get_field_id('child'),
                'name' => $this->get_field_name('child'),
                'value' => (isset($instance['child'])) ? 'true' : ''
            ),
        );
        $taxonomies = get_taxonomies(array('_builtin' => false), 'objects');
        ?>
        <p>
            <label for="<?php echo $field_data['title']['id']; ?>"><?php _e('Titolo:'); ?></label>
            <input class="widefat" id="<?php echo $field_data['title']['id']; ?>" name="<?php echo $field_data['title']['name']; ?>" type="text" value="<?php echo esc_attr($field_data['title']['value']); ?>">
        </p>


        <p style='font-weight: bold;'><?php _e('Options:'); ?></p>

        <p>
            <label for="<?php echo $field_data['max_terms']['id']; ?>"><?php _e('Numero massimo di elementi:'); ?></label>
            <input class="widefat" id="<?php echo $field_data['max_terms']['id']; ?>" name="<?php echo $field_data['max_terms']['name']; ?>" type="text" value="<?php echo esc_attr($field_data['max_terms']['value']); ?>" placeholder="Lascia vuoto per mostrare tutti">
        </p>

        <p>
            <label for="<?php echo $field_data['exclude']['id']; ?>"><?php _e('Id da escludere:'); ?></label>
            <input class="widefat" id="<?php echo $field_data['exclude']['id']; ?>" name="<?php echo $field_data['exclude']['name']; ?>" type="text" value="<?php echo esc_attr($field_data['exclude']['value']); ?>" placeholder="Separa id con una virgola ','">
        </p>

        <p>
            <label for="<?php echo $field_data['include']['id']; ?>"><?php _e('Only Display Terms With The Following Ids:'); ?></label>
            <input class="widefat" id="<?php echo $field_data['include']['id']; ?>" name="<?php echo $field_data['include']['name']; ?>" type="text" value="<?php echo esc_attr($field_data['include']['value']); ?>" placeholder="Separa id con una virgola ','">
        </p>

        <p style='font-weight: bold;'><?php _e('Tassonomie personalizzate:'); ?></p>

        <?php foreach ($taxonomies as $taxonomy): ?>
            <p>
                <input id="<?php echo $taxonomy->name; ?>" name="<?php echo $field_data['taxonomies']['name']; ?>[]" type="checkbox" value="<?php echo $taxonomy->name; ?>" <?php echo $this->is_taxonomy_checked($field_data['taxonomies']['value'], $taxonomy->name); ?>>
                <label for="<?php echo $taxonomy->name; ?>"><?php echo $taxonomy->labels->name; ?></label>
            </p>
        <?php endforeach; ?>
        <?php
    }

    public function update($new_instance, $old_instance) {
        $instance['title'] = strip_tags($new_instance['title']);
        $instance['max_terms'] = $new_instance['max_terms'];
        $instance['exclude'] = $new_instance['exclude'];
        $instance['include'] = $new_instance['include'];
        $instance['custom_taxonomies'] = $new_instance['custom_taxonomies'];
        return $instance;
    }

    public function is_taxonomy_checked($custom_taxonomies_checked, $taxonomy_name) {
        if (!is_array($custom_taxonomies_checked))
            return checked($custom_taxonomies_checked, $taxonomy_name);
        if (in_array($taxonomy_name, $custom_taxonomies_checked))
            return 'checked="checked"';
    }

}

add_action('widgets_init', 'init_custom_taxonomy_widget');

function init_custom_taxonomy_widget() {
    register_widget('CustomTaxonomyWidget');
}

/**
 * 
 * Widget Acerbo Social
 * 
 * @global type $post
 * @global type $wp_query
 * @global type $author
 * @param type $id
 * @param type $home
 * @param type $sep
 */
class acerbo_social_widget extends WP_Widget {

    function __construct() {
        parent::__construct(
                'acerbo_social_widget', 'Acerbo social', array('description' => 'Mostra i pulsanti social dell\'Acerbo')
        );
    }

    public function widget($args, $instance) {
        $acerbo_facebook = ot_get_option('acerbo_facebook', '');
        $acerbo_twitter = ot_get_option('acerbo_twitter', '');
        $acerbo_youtube = ot_get_option('acerbo_youtube', '');
	    $acerbo_instagram = ot_get_option('acerbo_instagram', '');
        $title = apply_filters('widget_title', $instance['title']);
        echo $args['before_widget'];
        if (!empty($title))
            echo $args['before_title'] . $title . $args['after_title'];
        echo '<div class="social-follow">';
        if ($acerbo_facebook) {
            echo '<a href="https://www.facebook.com/' . $acerbo_facebook . '" title="Seguici su Facebook" target="_blank"><span class="sr-only">Seguici su Facebook</span><i class="fa fa-facebook-square fa-3x"></i></a>';
        }
        if ($acerbo_twitter) {
            echo '<a href="https://www.twitter.com/' . $acerbo_twitter . '" title="Seguici su Twitter" target="_blank"><span class="sr-only">Seguici su Twitter</span><i class="fa fa-twitter-square fa-3x"></i></a>';
        }
        if ($acerbo_youtube) {
            echo '<a href="https://www.youtube.com/user/' . $acerbo_youtube . '" title="Seguici su Youtube" target="_blank"><span class="sr-only">Seguici su Youtube</span><i class="fa fa-youtube-square fa-3x"></i></a>';
        }
	    if ($acerbo_instagram) {
		    echo '<a href="https://www.instagram.com/' . $acerbo_instagram . '/?hl=it" title="Seguici su Instagram" target="_blank"><span class="sr-only">Seguici su Instagram</span><i class="fa fa-instagram fa-3x"></i></a>';
	    }
        echo '<a href="/feed" title="Seguici via RSS" target="_blank"><span class="sr-only">Seguici attraverso RSS</span><i class="fa fa-rss-square fa-3x"></i></a>';
        echo '</div>';

        echo $args['after_widget'];
    }

// Widget Backend 
    public function form($instance) {
        if (isset($instance['title'])) {
            $title = $instance['title'];
        } else {
            $title = 'Titolo';
        }
// Widget admin form
        ?>
        <p>
            <label for="<?php echo $this->get_field_id('title'); ?>">Titolo</label> 
            <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo esc_attr($title); ?>" />
        </p>
        <?php
    }

    public function update($new_instance, $old_instance) {
        $instance = array();
        $instance['title'] = (!empty($new_instance['title']) ) ? strip_tags($new_instance['title']) : '';
        return $instance;
    }

}

/**
 * 
 * Widget Acerbo Lifestream
 * 
 * @global type $post
 * @global type $wp_query
 * @global type $author
 * @param type $id
 * @param type $home
 * @param type $sep
 */
class acerbo_lifestream_widget extends WP_Widget {

    function __construct() {
        parent::__construct(
                'acerbo_lifestream_widget', 'Acerbo Social Stream', array('description' => 'Mostra gli ultimi contenti dei canali social dell\'Acerbo')
        );
    }

    public function widget($args, $instance) {
        $acerbo_facebook = ot_get_option('acerbo_facebook', '');
        $acerbo_twitter = ot_get_option('acerbo_twitter', '');
        $acerbo_youtube = ot_get_option('acerbo_youtube', '');
        $title = apply_filters('widget_title', $instance['title']);
        $randomId = substr(str_shuffle("abcdefghijklmnopqrstuvwxyz"), 0, 6);
        $list = array();
        echo $args['before_widget'];
        if (!empty($title)) {
            echo $args['before_title'] . $title . $args['after_title'];
        }
        echo '<div id="' . $randomId . '" class="lifestream"></div>';
        if ($acerbo_facebook) {
            $list[] = '{service: "facebook_page",user: "' . $acerbo_facebook . '",template: {wall_post: \'Dalla bacheca: <a href="${link}" target="_blank">${title}</a>\'}}';
        }
        if ($acerbo_twitter) {
            $list[] = '{service: "twitter",user: "' . $acerbo_twitter . '",template: {posted: \'{{html tweet}}\'}}';
        }
        $listFiltered = array_filter($list);
        if (!empty($listFiltered)) {
            $listSingle = implode(",", $listFiltered);
            echo '<script>function runLifestream() {{$("#' . $randomId . '").lifestream({limit:5,list:[' . $listSingle . ']});}};</script>';
        }

        echo $args['after_widget'];
    }

// Widget Backend 
    public function form($instance) {
        if (isset($instance['title'])) {
            $title = $instance['title'];
        } else {
            $title = 'Titolo';
        }
// Widget admin form
        ?>
        <p>
            <label for="<?php echo $this->get_field_id('title'); ?>">Titolo</label> 
            <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo esc_attr($title); ?>" />
        </p>
        <?php
    }

    public function update($new_instance, $old_instance) {
        $instance = array();
        $instance['title'] = (!empty($new_instance['title']) ) ? strip_tags($new_instance['title']) : '';
        return $instance;
    }

}

function acerbo_load_widget() {
    register_widget('acerbo_social_widget');
    register_widget('acerbo_lifestream_widget');
}

add_action('widgets_init', 'acerbo_load_widget');
