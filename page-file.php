<style>
    p.doctitle {
        border-bottom: 1px solid gainsboro;
        margin: 0;
        padding: 10px 0;
    }
    .filelist:first-child {
        border-top: 1px solid gainsboro;
    }

    .pagination>li>a>i {
        line-height: 1.42857143;
    }
    
    .pagination span.extend {
        display: none;
    }        
</style>
<?php
/*
  Template Name: Template per i file
 */

get_header();

// set up or arguments for our custom query
$paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
$pdf_mime = array("application/pdf");
$doc_mime = array("application/msword", "application/vnd.openxmlformats-officedocument.wordprocessingml.document", "application/vnd.openxmlformats-officedocument.wordprocessingml.template", "application/vnd.ms-word.document.macroEnabled.12", "application/vnd.ms-word.template.macroEnabled.12");
$xls_mime = array("application/vnd.ms-excel", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "application/vnd.openxmlformats-officedocument.spreadsheetml.template", "application/vnd.ms-excel.sheet.macroEnabled.12", "application/vnd.ms-excel.template.macroEnabled.12", "application/vnd.ms-excel.addin.macroEnabled.12", "application/vnd.ms-excel.sheet.binary.macroEnabled.12");
$ppt_mime = array("application/vnd.ms-powerpoint", "application/vnd.openxmlformats-officedocument.presentationml.presentation", "application/vnd.openxmlformats-officedocument.presentationml.template", "application/vnd.openxmlformats-officedocument.presentationml.slideshow", "application/vnd.ms-powerpoint.addin.macroEnabled.12", "application/vnd.ms-powerpoint.presentation.macroEnabled.12", "application/vnd.ms-powerpoint.slideshow.macroEnabled.12");
$oo_doc_mime = array("application/vnd.oasis.opendocument.text", "application/vnd.oasis.opendocument.text-template", "application/vnd.oasis.opendocument.text-web", "application/vnd.oasis.opendocument.text-master");
$oo_xls_mime = array("application/vnd.oasis.opendocument.spreadsheet", "application/vnd.oasis.opendocument.spreadsheet-template");
$oo_ppt_mime = array("application/vnd.oasis.opendocument.presentation", "application/vnd.oasis.opendocument.presentation-template");
$oo_draw_mime = array("application/vnd.oasis.opendocument.graphics", "application/vnd.oasis.opendocument.graphics-template");
$archive_mime = array("application/zip", "application/x-gzip", "application/rar", "application/7z");
$mime = array_merge($pdf_mime, $doc_mime, $xls_mime, $ppt_mime, $oo_doc_mime, $oo_xls_mime, $oo_ppt_mime, $oo_draw_mime, $archive_mime);
$query_args = array(
    'post_type' => 'attachment',
    'posts_per_page' => 40,
    'post_status' => 'inherit',
    'post_mime_type' => $mime,
    'orderby' => 'post_date',
    'order' => 'DESC',
    'paged' => $paged
);
// create a new instance of WP_Query
$the_query = new WP_Query($query_args);
?>

<div id="content" class="col-md-8 bd-right">

    <h1 class="title compensate-bs"><?php the_title(); ?></h1>

    <div id="post-<?php the_ID(); ?>" <?php post_class('archive-bodycopy'); ?>>

        
    <div>
        <form method="get" class="searchform" action="/serp">
            <div class="input-group">
                <label class="sr-only" for="modulodiricercamobile">Modulo di ricerca</label>
                <input id="modulodiricercamobile" type="text" class="form-control input-sm" name="q" placeholder='Scrivi il testo e premi Cerca' value="">
                <div class="input-group-btn">
                    <button type="submit" class="btn btn-primary btn-sm"><i class="fa fa-search fa-lg"></i>&nbsp;<span class="hidden-xs">Cerca</span></button>
                </div>
            </div>
        </form>
    </div>
        
        <div class="filelist post-bodycopy">
            <?php
            if ($the_query->have_posts()) : while ($the_query->have_posts()) : $the_query->the_post();
                    $type = get_post_mime_type($post->id);
                    if ($type == 'application/pdf') {
                        $html = 'pdf';
                    } elseif (in_array($type, $archive_mime)) {
                        $html = 'archive';
                    } elseif (in_array($type, $doc_mime)) {
                        $html = 'word';
                    } elseif (in_array($type, $xls_mime)) {
                        $html = 'excel';
                    } elseif (in_array($type, $ppt_mime)) {
                        $html = 'powerpoint';
                    } elseif (in_array($type, $oo_doc_mime)) {
                        $html = 'word';
                    } elseif (in_array($type, $oo_xls_mime)) {
                        $html = 'excel';
                    } elseif (in_array($type, $oo_ppt_mime)) {
                        $html = 'powerpoint';
                    } elseif (in_array($type, $oo_draw_mime)) {
                        $html = 'image';
                    } else {
                        $html = 'archive';
                    }

                    $content = wp_get_attachment_url($post->id);
                    $parse = parse_url($content);
                    $filePath = $parse['path'];
                    $link = get_attachment_link();
                    ?>
                    <p class="doctitle"><span class="fa-stack"><i class="fa fa-square fa-stack-2x fa-color-<?php echo $html; ?>"></i><i class="fa fa-file-<?php echo $html; ?>-o fa-stack-1x fa-inverse"></i></span>&nbsp;&nbsp;<a href="<?php echo $link; ?>" target="_blank"><?php echo get_the_title(); ?></a>
                        <span class="hidden-xs doc__button pull-right">
                            <a href="http://drive.google.com/viewerng/viewer?url=<?php echo $content; ?>" target="_blank" class="nofancybox"><button type="button" class="btn btn-xs" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Visualizza in una nuova finestra"><i class="fa fa-external-link"></i></button></a>
                            <a href="<?php echo $content ?>" target="_blank" class="nofancybox" title="Scarica il file" download><button type="button" class="btn btn-xs htmldownload" style="display: inline-block;"><i class="fa fa-download" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Scarica il file"></i></button></a>
                            <a href="dir=<?php echo ltrim(dirname($filePath), '/') ?>&file=<?php echo basename($filePath); ?>" class="nofancybox" title="Scarica il file"><button type="button" class="btn btn-xs phpdownload" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Scarica il file"><i class="fa fa-download"></i></button></a>
                        </span>
                    </p>

                    <?php
                endwhile;
                ?>
            </div>


            <?php if ($the_query->max_num_pages > 1) { // check if the max number of pages is greater than 1      ?>
                <?php if (function_exists('wp_pagenavi')) : ?>
                    <?php wp_pagenavi(array('query' => $the_query)); ?>
                <?php else : ?>
                    <div class="alignleft"><?php next_posts_link('Contenuti più vecchi'); ?></div>
                    <div class="alignright"><?php previous_posts_link('Contenuti più nuovi'); ?></div>
                <?php endif; ?>
            <?php } ?>
        <?php endif; ?>

    </div>

</div>


<div id="widgetarea-one" class="col-md-4 bd-left-minus">
    <h1 class="title compensate-bs" style="margin-bottom: 25px">Sezioni</h1>
    <?php get_template_part('sidebar', 'archive'); ?>     
</div>    
<?php get_footer(); ?>

