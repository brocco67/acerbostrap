<?php
get_header();
$permalink = urlencode( get_permalink() );
?>

    <div id="content" class="col-md-8 bd-right">
		<?php
		// Start the Loop.
		while ( have_posts() ) :
		the_post();
		?>
        <div id="post-<?php the_ID(); ?>" <?php post_class( 'acerbo-entry' ); ?>>

			<?php
			if ( has_post_thumbnail() ) {
				echo '<div class="post-thumb compensate-bs"><div class="thumb-inner">';
				if ( has_post_thumbnail() ) {
					$imgId   = get_post_thumbnail_id( $post->ID );
					$imgFull = wp_get_attachment_image_src( $imgId, 'full' );
					echo '<picture>'
					     . '<!--[if IE 9]><video style="display: none;"><![endif]-->'
					     . '<source media="(min-width: 992px)" data-srcset="' . $imgFull[0] . '?width=777&height=280&enhance=true">'
					     . '<source media="(min-width: 415px)" data-srcset="' . $imgFull[0] . '?width=777&height=186&enhance=true">'
					     . '<!--[if IE 9]></video><![endif]-->'
					     . '<img src="' . $imgFull[0] . '?width=414&height=177&enhance=true" alt="' . trim( strip_tags( get_post_meta( $imgId, '_wp_attachment_image_alt', true ) ) ) . '" class="lazyload img-responsive" heigth="0" width="0">'
					     . '</picture>';
				}
				echo '</div></div>';
			}
			?>
            <div class="page-header">
                <h1 class="title compensate-bs">
					<?php the_title(); ?>
                </h1>
            </div>

			<?php if ( is_singular( 'contributo' ) ) : ?>
                <div class="post-author">
                    <p>di <?php echo tbm_get_the_author(); ?></p>
                </div>
			<?php endif; ?>

            <div class="post-bodycopy">
				<?php if ( $post->post_type == 'circolare' ) { ?>
                    <div class="lead">Circolare <?php echo CFS()->get( 'circolare_num' ) ?>
                        del <?php echo date( 'j/m/Y', strtotime( CFS()->get( 'circolare_data' ) ) ); ?></div>
				<?php } elseif ( $post->post_type == 'avviso' ) { ?>
                    <div class="lead">Avviso, <?php echo CFS()->get( 'avviso_num' ) ?>
                        del <?php echo date( 'j/m/Y', strtotime( CFS()->get( 'avviso_data' ) ) ); ?></div>
				<?php } ?>
				<?php the_content(); ?>
				<?php
				wp_link_pages( array(
					'before' => __( '<p class="post-pagination">Pages:', 'montezuma' ),
					'after'  => '</p>'
				) );
				?>
            </div>
            <!--div class="alert alert-info" role="alert">Se vuoi ricevere gli aggiornamenti di questo sito via posta elettronica <a href="/newsletter" class="alert-link">iscriviti alla nostra newsletter</a>.</div-->
            <hr class="styled"/>
            <div class="post-share">
                <span class="hidden-xs">Condividi:</span>
                <a href="https://telegram.me/share/url?url=<?php echo $permalink; ?>&text=<?php echo urlencode( get_the_title() ); ?>"
                   title="Condividi su Telegram" class="share-link visible-xs-inline-block"><i
                            class="fa fa-telegram fa-lg icon-telegram"></i></a>
                <a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo $permalink; ?>"
                   title="Condividi su Facebook" class="wn-pop share-link"><i
                            class="fa fa-facebook fa-lg icon-facebook"></i></a>
                <a href="https://twitter.com/share?url=<?php echo $permalink; ?>&via=acerbosocial&text=<?php echo urlencode( get_the_title() ); ?>"
                   title="Condividi su Twitter" class="wn-pop share-link"><i
                            class="fa fa-twitter fa-lg icon-twitter"></i></a>
                                <a href="<?php echo $permalink; ?>"
                   title="Condividi su Whatsapp" class="whatsapp-share share-link visible-xs-inline-block"><i
                            class="fa fa-whatsapp fa-lg icon-whatsapp "></i></a>
            </div>
            <div class="post-footer">
				<?php
				echo '<span><i class="fa fa-clock-o fa-fw"></i>&nbsp;Pagina pubblicata il ' . get_the_time( 'j F Y' ) . '</span>';
				echo get_the_term_list( $post->ID, 'category', '<span><i class="fa fa-bookmark fa-fw"></i>&nbsp;Categorie:', '&middot;', '</span>' );
				echo get_the_term_list( $post->ID, 'tema', '<span><i class="fa fa-bookmark fa-fw"></i>&nbsp;Temi:', '&middot;', '</span>' );
				echo get_the_term_list( $post->ID, 'post_tag', '<span><i class="fa fa-tags fa-fw"></i>&nbsp;Tag:', '&middot;', '</span>' );
				the_image_credits( '', '<span><i class="fa fa-picture-o fa-fw"></i>&nbsp;Immagine: ', '</span>' );
				?>
            </div>

			<?php
			if ( comments_open() || get_comments_number() ) {
				echo '<hr class="styled" />';
				comments_template();
			}
			?>
            <hr class="styled"/>
            <nav class="singlenav clearfix">
                <div class="older"><?php previous_post_link(); ?></div>
                <div class="newer"><?php next_post_link(); ?></div>
            </nav>

			<?php
			endwhile;
			?>

        </div>
    </div>

    <div id="widgetarea-one" class="col-md-4 bd-left-minus">
        <h1 class="title compensate-bs" style="margin-bottom: 25px">Sezioni</h1>
		<?php get_template_part( 'sidebar', 'archive' ); ?>
    </div>
<?php get_footer(); ?>