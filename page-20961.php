<?php
/**
 * Header del sito
 */
get_header();
while ( have_posts() ) : the_post();
	?>
    <style>
        h1, h2, h3, h4 {
            color: #3a5e3b
        }
    </style>
    <h1 class="title" style="background-color: #3a5e3b;color: #fff;"><?php the_title(); ?></h1>


    <!-- CONTENUTO CENTRALE -->
    <div id="content" class="cf col-md-8 bd-right">

        <!-- QUI VA HTML PERSONALIZZATO -->
			<h3>OPEN DAYS</h3>
      <p>nbla bla bla</p>
      <div class="row">
        <div class="col-md-3">         
		<div class="jumbotron">
        <h2>6 Dicembre 2020</h2>
        <p class="lead">Open Day</p>
        <p><a class="btn btn-lg btn-success" href="#" role="button">Prenota</a></p>
      </div>
	  </div>
        <div class="col-md-3">
				<div class="jumbotron">
        <h2>13 Dicembre 2020</h2>
        <p class="lead">Open Day</p>
        <p><a class="btn btn-lg btn-success" href="#" role="button">Prenota</a></p>
      </div>
		</div>
        <div class="col-md-3">
						<div class="jumbotron">
        <h2>16 Gennaio 2021</h2>
        <p class="lead">Open Day</p>
        <p><a class="btn btn-lg btn-success" href="#" role="button">Prenota</a></p>
      </div>
		
		
		</div>
		        <div class="col-md-3">
						<div class="jumbotron">
        <h2>17 Gennaio 2021</h2>
        <p class="lead">Open Day</p>
        <p><a class="btn btn-lg btn-success" href="#" role="button">Prenota</a></p>
      </div>
		
		
		</div>
      </div>

		<?php
		/**
		 * Contenuto pubblicato in Wordpress
		 */
		the_content();
		?>


    </div>
    <!-- FINE CONTENUTO CENTRALE -->
<?php endwhile; ?>


    <!-- SIDEBAR DESTRA -->
    <div id="widgetarea-one" class="col-md-4 bd-left-minus">

		<?php
		get_sidebar( 'orientamento' );
		?>

    </div>
    <!-- FINE SIDEBAR DESTRA -->


<?php
/**
 * FOOTER del sito
 */
get_footer();
?>