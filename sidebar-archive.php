<?php
if (is_tax() || is_category()) :
    $term = get_queried_object();
    $children = get_term_children($term->term_id, $term->taxonomy);
    if ($children) :
        ?>

        <aside id="custom_taxonomy_widget-2" class="clearfix" style="margin-bottom: 30px">
            <div class="dropdown">
                <button class="btn btn-lg btn-info dropdown-toggle btn-block btn-widget" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-expanded="true">
                    Altro in <?php echo $term->name; ?>
                    <span class="caret"></span>
                </button>
                <ul class="dropdown-menu" style="width:100%"role="menu" aria-labelledby="dropdownMenu1">
                    <?php
                    $i = 0;
                    foreach ($children as $child) :
                        $childTerm = get_term_by('id', $child, $term->taxonomy);
                        if ($childTerm->count > 0) {
                            echo '<li role="presentation" ><a role="menuitem" tabindex="-1" href="' . get_term_link($child, $term->taxonomy) . '">' . $childTerm->name . '</a></li>';
                            if ($i < $childTerm->count - 1) {
                                echo '<li class="divider"></li>';
                            }
                            $i++;
                        }
                    endforeach;
                    ?>
                </ul>
            </div>
        </aside>
        <?php
    endif;
endif;

if (is_page()) :
    $query00 = new WP_Query(
            array(
        'post_type' => 'page',
        'post_parent' => get_the_ID(),
        'orderby' => 'menu_order',
        'order' => 'ASC'
            )
    );
    if ($query00->have_posts()) :
        ?>

        <aside id="custom_taxonomy_widget-2" class="clearfix" style="margin-bottom: 30px">
            <div class="dropdown">
                <button class="btn btn-lg btn-info dropdown-toggle btn-block btn-widget" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-expanded="true">
                    Altro in <?php echo $post->post_title; ?>
                    <span class="caret"></span>
                </button>
                <ul class="dropdown-menu" style="width:100%"role="menu" aria-labelledby="dropdownMenu1">

                    <?php
                    $counter = $query00->post_count;
                    $i = 0;
                    while ($query00->have_posts()) :
                        $query00->the_post();
                        echo '<li role="presentation" ><a role="menuitem" tabindex="-1" href="' . get_the_permalink() . '">' . get_the_title() . '</a></li>';
                        if ($i < $counter - 1) {
                            echo '<li class="divider"></li>';
                        }
                        $i++;
                    endwhile;
                    ?>
                </ul>
            </div>
        </aside>
        <?php
    else :

    endif;
    wp_reset_postdata();
endif;


if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('sidebar-1')) :
endif;