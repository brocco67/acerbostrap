    <div id="post-<?php the_ID(); ?>" <?php post_class('cf'); ?>>

        <h2>
            <span class="post-format"></span>
            <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>" rel="bookmark"><?php the_title(); ?></a>
        </h2>

        <div class="post-bodycopy cf">

            <div class="post-date">		
                <p class="post-day"><?php the_time('j'); ?></p>
                <p class="post-month"><?php the_time('M'); ?></p>
                <p class="post-year"><?php the_time('Y'); ?></p>				
            </div>

            <?php the_excerpt(); ?>

        </div>

        <div class="post-footer">
            <a class="post-readmore" href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">Leggi tutto &rarr;</a>
            <p class="post-categories"><?php the_category(' &middot; '); ?></p>

            <?php the_tags('<p class="post-tags">', ' &middot; ', '</p>'); ?>
        </div>

    </div>