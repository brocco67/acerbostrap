<?php

echo '';
echo '<style>            

.cbp-spmenu {
    display: block;
    -webkit-overflow-scrolling: touch;
    width: 100%;

}
.cbp-spmenu .menu-item-has-children ul { padding-left: 20px }


.cbp-spmenu h3 {
    color: #afdefa;
    font-size: 1.9em;
    padding: 20px;
    margin: 0;
    font-weight: 300;
    background: #0d77b6;
}
.cbp-spmenu a {
    display: block;
    color: #333;
    font-size: 1.3em;
    font-weight: 300;
    font-family: "Oswald", "Segoe UI", sans-serif;
    text-decoration: none;
}
.cbp-spmenu a:active,
.cbp-spmenu a:hover,
.cbp-spmenu a:focus { color: #333; text-decoration: none; }
.cbp-spmenu li { display: block }
.cbp-spmenu-vertical a {
    border-bottom: 1px solid chocolate;
    padding: 1em;
}
</style>'
?>