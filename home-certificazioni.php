<div id="certificazioni" class="fastlink col-sm-12">
    <h1 class="title compensate-bs">Certificazioni</h1>
    <?php
    if (function_exists('ot_get_option')) {
        $certificazioniText = ot_get_option('certificazioni_text', array());
        if (!empty($certificazioniText)) {
            $doc = new DOMDocument();
            $doc->loadHTML('<meta http-equiv="content-type" content="text/html; charset=utf-8">'.$certificazioniText);
            $textareas = $doc->getElementsByTagName('p');
            foreach ($textareas as $textarea) {
                append_attr_to_element($textarea, 'class', 'lead');
            }
            echo $doc->saveHTML();
        }
    }
    ?>

    <div class="owl-certificazioni owl-carousel">

        <?php
        if (function_exists('ot_get_option')) {
            $certificazionis = ot_get_option('certificazioni', array());
            if (!empty($certificazionis)) {
                foreach ($certificazionis as $certificazioni) {
                    if (!empty($certificazioni['image'])) {
                        $imgId = acerbo_get_image_id($certificazioni['image']);
                        $imgSrc = wp_get_attachment_image($imgId, 'thumb', '', array(
                            'class' => "media-object img-thumbnail",
                            'alt' => trim(strip_tags(get_post_meta($attachment_id, '_wp_attachment_image_alt', true))),
                        ));
                    } else {
                        $imgSrc = '<img class="media-object" src="http://placehold.it/150x90" alt="Immagine segnaposto">';
                    }
                    echo '<div class="media"><div class="media-left media-middle"><a href="' . $certificazioni['link'] . '">' . $imgSrc . '</a></div><div class="media-body"> <a href="' . $certificazioni['link'] . '"><h3 class="media-heading">' . $certificazioni['title'] . '</h3></a>' . $certificazioni['description'] . ' </div> </div>';
                }
            }
        }
        ?>


    </div>
</div>
<div class="clearfix"></div>