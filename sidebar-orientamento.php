<aside id="strip" class="widget clearfix">
    <img class="img-responsive"
         src="<?php echo get_template_directory_uri(); ?>/images/orientamento_2018_02.jpg"
         alt="Banner dell'Acerbo raffigurante un pollice con i like"/>
</aside>
<aside id="strip" class="widget clearfix">
    <img class="img-responsive"
         src="<?php echo get_template_directory_uri(); ?>/images/orientamento_2018_01.jpg"
         alt="banner dell'Acerbo raffigurante una mela verde e la scritta mordi il tuo futuro"/>
</aside>

<aside id="acerbo_social_widget-2" class="widget widget_acerbo_social_widget clearfix"><h3><i
                class="fa fa-caret-right"></i>&nbsp;<span>Segui l'Acerbo</span></h3>

    <div class="social-follow"><a href="https://www.facebook.com/311961392258" title="Seguici su Facebook"
                                  target="_blank"><span class="sr-only">Seguici su Facebook</span><i
                    class="fa fa-facebook-square fa-3x"></i></a><a href="https://www.twitter.com/acerbosocial"
                                                                   title="Seguici su Twitter"
                                                                   target="_blank"><span
                    class="sr-only">Seguici su Twitter</span><i class="fa fa-twitter-square fa-3x"></i></a>
        <a href="https://www.youtube.com/user/acerbochannel" title="Seguici su Youtube" target="_blank">
            <span class="sr-only">Seguici su Youtube</span><i class="fa fa-youtube-square fa-3x"></i></a>
        <a href="/feed" title="Seguici via RSS" target="_blank">
            <span class="sr-only">Seguici attraverso RSS</span><i class="fa fa-rss-square fa-3x"></i>
        </a>
        <a href="https://www.instagram.com/acerbosocialpescara/?hl=it" title="Seguici su Instagram"
           target="_blank">
            <span class="sr-only">Seguici su Instagram</span><i class="fa fa-instagram fa-3x"></i>
        </a>
    </div>
</aside>