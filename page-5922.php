<?php
/**
 * Header del sito
 */
get_header();
while ( have_posts() ) : the_post();
	?>
    <style>
        h1, h2, h3, h4 {
            color: #3a5e3b
        }
    </style>
    <h1 class="title" style="background-color: #3a5e3b;color: #fff;"><?php the_title(); ?></h1>

	<?php
	/**
	 * Video dell'Acerbo
	 */
	get_template_part( 'home', 'video' );
	?>

    <!-- CONTENUTO CENTRALE -->
    <div id="content" class="cf col-md-8 bd-right">

        <h2>Vieni a scoprire l'Acerbo</h2>

        <p>Per conoscere da vicino l'Istituto Acerbo vi aspettiamo numerosi nelle domeniche di scuola aperta, nei
            laboratori settimanali del martedì, mercoledì e giovedì "una giornata all'Acerbo", nei sabato di gennaio
            dedicati agli incontri col dirigente scolastico, nelle iniziative di informazione-formazione rivolte a
            docenti e genitori.</p>

        <h3>Open Days 2017 / 2018</h3>
        <ul>
            <li><strong>Dicembre 2017</strong>: domenica 17, dalle 10 alle 13.</li>
            <li><strong>Gennaio 2018</strong>: tutte le domeniche a partire da domenica 14; dalle 10 alle 13 e dalle 15
                alle 17.
            </li>
            <li><strong>Febbraio 2018</strong>: domenica 4, dalle 10 alle 13 e dalle 15 alle 17.</li>

        </ul>

        <h3>L'Acerbo compie 95 anni</h3>
        <p>Nel <strong>pomeriggio di sabato 20 gennaio 2018</strong> l'Acerbo celebrerà i suoi 95 anni con varie
            iniziative culturali, scientifiche, laboratoriali e la partecipazione di tutta la comunità scolastica e di
            personalità del mondo delle professioni, dell'università, dell'arte e dello spettacolo.</p>

        <h3>Attività di laboratorio mattutine</h3>
        <p>L'Istituto Tecnico "Tito Acerbo" promuove attività laboratoriali mattutine:</p>
        <ul>
            <li>per gli indirizzi ECONOMICO e TURISTICO, nelle discipline di Informatica, Economia e Lingue straniere.
            </li>
            <li>per l'indirizzo COSTRUZIONI, AMBIENTE E TERRITORIO, nelle discipline Progettazione Tecnica in Autocad,
                Topografia, Informatica e Fisica.
            </li>
        </ul>
        <p>Le attività si svolgeranno nella nostra Sede dalle ore 8,30 alle 13,00 secondo il seguente calendario :</p>
        <ul>
            <li>12, 14, 15 dicembre 2017</li>
            <li>9, 11, 12 gennaio 2018</li>
            <li>16, 18, 19 gennaio 2018</li>
            <li>23, 25, 26 gennaio 2018</li>
            <li>1, 2 febbraio 2018</li>
        </ul>
        <p>Saranno costituiti gruppi di max 20 alunni che dovranno prenotarsi almeno sette giorni prima, tramite il
            docente referente della loro scuola.</p>

        <div style="padding: 20px 0 40px 0" class="text-center"><p><a class="btn btn-primary btn-lg"
                                                                      href="http://www.istitutotecnicoacerbope.edu.it/scuola/urp/contatti/"
                                                                      role="button">Contattaci o vienici a trovare!</a>
            </p></div>

		<?php
		/**
		 * Contenuto pubblicato in Wordpress
		 */
		the_content();
		?>

        <h2>L'Istituto tecnico "Tito Acerbo"</h2>

        <div class="row">
            <div class="col-xs-12">
                <div class="grid-item">
                    <picture>
                        <!--[if IE 9]>
                        <video style="display: none;"><![endif]-->
                        <source media="(min-width: 992px)"
                                data-srcset="https://istitutoacerbo.appspot.com/images/2014/12/img-00951.png?height=150&width=747&enhance=true">
                        <source media="(min-width: 768px)"
                                data-srcset="https://istitutoacerbo.appspot.com/images/2014/12/img-00951.png?height=230&width=768&enhance=true">
                        <source media="(min-width: 414px)"
                                data-srcset="https://istitutoacerbo.appspot.com/images/2014/12/img-00951.png?height=125&width=768&enhance=true">
                        <!--[if IE 9]></video><![endif]--><img
                                src="https://istitutoacerbo.appspot.com/images/2014/12/img-00951.png?height=177&width=345&enhance=true"
                                alt="" class="lazyload img-responsive">
                    </picture>
                    <div class="grid-paragraph" style="padding: 20px 10px">
                        <p>La scuola superiore più antica di Pescara,a due passi dalla stazione ferroviaria e
                            dal capolinea di tutti gli autobus urbani ed extraurbani, dal 1923 garantisce agli
                            studenti provenienti dalla città e da tutta la Provincia, una formazione di qualità
                            negli studi di economia e marketing, ambiente e territorio, lingue e turismo.</p>

                        <!--p><strong>Scopri l'Acerbo</strong>: <a href="/scuola/i-laboratori/">Laboratori e Strutture</a>
                            • <a href="/offerta-formativa/certificazioni/">Le certificazioni</a> • <a href="/docenti-ata/5425-alternanza-scuola-lavoro-a-s-2015-20106/">Alternanza Scuola-Lavoro</a> • <a href="/studenti/5607-attivazione-corsi-per-lampliamento-dellofferta-formativa/">I corsi formativi</a></p-->
                    </div>

                </div>
            </div>
        </div>


        <h2>I corsi di studio</h2>

        <div class="row">
            <div class="col-md-4">
                <div class="grid-item">
                    <picture>
                        <!--[if IE 9]>
                        <video
                                style="display: none;"><![endif]-->
                        <source media="(min-width: 992px)"
                                data-srcset="https://istitutoacerbo.appspot.com/images/img_cat.png?height=125&width=262&enhance=true">
                        <source media="(min-width: 768px)"
                                data-srcset="https://istitutoacerbo.appspot.com/images/img_cat.png?height=230&width=768&enhance=true">
                        <source media="(min-width: 414px)"
                                data-srcset="https://istitutoacerbo.appspot.com/images/img_cat.png?height=125&width=768&enhance=true">
                        <!--[if IE 9]></video><![endif]--><img
                                src="https://istitutoacerbo.appspot.com/images/img_cat.png?height=177&width=345&enhance=true"
                                alt="" class="lazyload img-responsive">
                    </picture>
                    <div class="grid-paragraph" style="padding-bottom: 20px" data-card="orientamento">
                        <h3 data-updated="" data-published="1443293073"><a
                                    href="/corsi-di-studio/costruzioni-ambiente-e-territorio"
                                    class="thumb-container">Costruzioni, Ambiente e Territorio</a>
                        </h3>

                        <p>Gli ex geometri, per imparare, con l'ausilio di laboratori e insegnanti tecnici, a
                            progettare edifici e altre strutture grazie al disegno tecnico digitale CAD.</p>

                        <h4>Articolazioni</h4>
                        <ul>

                            <li>
                                <a href="/corsi-di-studio/costruzioni-ambiente-e-territorio/tecnico-delle-costruzioni-in-legno/">Tecnico
                                    delle Costruzioni in Legno</a></li>
                        </ul>
                    </div>

                </div>
            </div>

            <div class="col-md-4">
                <div class="grid-item">
                    <picture>
                        <!--[if IE 9]>
                        <video
                                style="display: none;"><![endif]-->
                        <source media="(min-width: 992px)"
                                data-srcset="https://istitutoacerbo.appspot.com/images/img_afm.png?height=125&width=262&enhance=true">
                        <source media="(min-width: 768px)"
                                data-srcset="https://istitutoacerbo.appspot.com/images/img_afm.png?height=230&width=768&enhance=true">
                        <source media="(min-width: 414px)"
                                data-srcset="https://istitutoacerbo.appspot.com/images/img_afm.png?height=125&width=768&enhance=true">
                        <!--[if IE 9]></video><![endif]--><img
                                src="https://istitutoacerbo.appspot.com/images/img_afm.png?height=177&width=345&enhance=true"
                                alt="" class="lazyload img-responsive">
                    </picture>
                    <div class="grid-paragraph" style="padding-bottom: 20px" data-card="orientamento">
                        <h3 data-updated="" data-published="1443293073"><a
                                    href="/corsi-di-studio/amministrazione-finanza-e-marketing"
                                    class="thumb-container">Amministrazione, finanza e
                                marketing</a>
                        </h3>

                        <p>Per acquisire competenze nel campo dei macrofenomeni economici nazionali ed
                            internazionali, della normativa civilistica e fiscali, dei sistemi e processi
                            aziendali e degli strumenti di marketing.</p>

                        <h4>Articolazioni</h4>
                        <ul>
                            <li>
                                <a href="/corsi-di-studio/amministrazione-finanza-e-marketing/sistemi-informativi-aziendali/">
                                    Sistemi Informativi Aziendali</a></li>
                            <li>
                                <a href="/corsi-di-studio/amministrazione-finanza-e-marketing/relazioni-internazionali-per-il-marketing/">Relazioni
                                    Internazionali per il Marketing</a></li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="col-md-4">
                <div class=" grid-item">
                    <picture>
                        <!--[if IE 9]>
                        <video
                                style="display: none;"><![endif]-->
                        <source media="(min-width: 992px)"
                                data-srcset="https://istitutoacerbo.appspot.com/images/img_afm.png?height=125&width=262&enhance=true">
                        <source media="(min-width: 768px)"
                                data-srcset="https://istitutoacerbo.appspot.com/images/img_afm.png?height=230&width=768&enhance=true">
                        <source media="(min-width: 414px)"
                                data-srcset="https://istitutoacerbo.appspot.com/images/img_afm.png?height=125&width=768&enhance=true">
                        <!--[if IE 9]></video><![endif]--><img
                                src="https://istitutoacerbo.appspot.com/images/img_afm.png?height=177&width=345&enhance=true"
                                alt="" class="lazyload img-responsive">
                    </picture>
                    <div class="grid-paragraph" style="padding-bottom: 20px" data-card="orientamento">
                        <h3 data-updated="" data-published="1443293073"><a
                                    href="/corsi-di-studio/turismo"
                                    class="thumb-container"
                            >Turismo</a>
                        </h3>

                        <p>Competenze specifiche nel comparto delle imprese del settore turistico e competenze
                            generali nel campo dei macrofenomeni economici nazionali ed internazionali, della
                            normativa civilistica e fiscale, dei sistemi aziendali.</p>
                    </div>

                </div>
            </div>

        </div>


    </div>
    <!-- FINE CONTENUTO CENTRALE -->
<?php endwhile; ?>


    <!-- SIDEBAR DESTRA -->
    <div id="widgetarea-one" class="col-md-4 bd-left-minus">

		<?php
		get_sidebar( 'orientamento' );
		?>

    </div>
    <!-- FINE SIDEBAR DESTRA -->


<?php
/**
 * FOOTER del sito
 */
get_footer();
?>