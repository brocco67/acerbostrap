<?php
$zaccordionPostId = array(
	ot_get_option( 'hp_post_01' ),
	ot_get_option( 'hp_post_02' ),
	ot_get_option( 'hp_post_03' ),
	ot_get_option( 'hp_post_04' ),
	ot_get_option( 'hp_post_05' ),
	ot_get_option( 'hp_post_06' )
);
if ( count( array_filter( array_unique( $zaccordionPostId ) ) ) === 6 ) {
	$query01 = array(
		'numberposts' => 6,
		'post_type'   => array( 'page', 'post', 'circolare' ),
		'post__in'    => $zaccordionPostId,
		'orderby'     => 'post__in'
	);
} else {
	$query01 = array(
		'numberposts' => 6,
		'post_type'   => array( 'page', 'post', 'circolare' ),
		'meta_key'    => 'sticked',
		'meta_value'  => '1'
	);
}
$mypost = get_posts( $query01 );
?>
<div id="inprimopiano" class="fastlink col-sm-12">
	<h1 class="title compensate-bs">In primo piano</h1>

	<div class="row grid-container">
		<?php
		foreach ( $mypost as $key => $post ) :
			setup_postdata( $post );
			if ( $key == 2 ) {
				echo '<div class="clearfix"></div>';
			}

			if ( $key == 0 || $key == 1 ) {
				$gridClass = 'col-md-6';
				$itemClass = 'grid-item grid-item--width2';
				$dataDom   = 'primopianobig';
				if ( has_post_thumbnail() ) {
					$imgId   = get_post_thumbnail_id( $post->ID );
					$imgFull = wp_get_attachment_image_src( $imgId, 'full' );
					$imgEcho = '<a href="' . get_permalink() . '"><picture>'
					           . '<!--[if IE 9]><video style="display: none;"><![endif]-->'
					           . '<source media="(min-width: 992px)" data-srcset="' . $imgFull[0] . '?width=554&height=200&enhance=true">'
					           . '<source media="(min-width: 415px)" data-srcset="' . $imgFull[0] . '?width=777&height=186&enhance=true">'
					           . '<!--[if IE 9]></video><![endif]-->'
					           . '<img src="' . $imgFull[0] . '?width=414&height=177&enhance=true" alt="' . trim( strip_tags( get_post_meta( $imgId, '_wp_attachment_image_alt', true ) ) ) . '" class="lazyload img-responsive">'
					           . '</picture></a>';

				} else {
					$image_attributes = wp_get_attachment_image_src( 1802, 'sticked' );
					$imgEcho          = '<img src="' . $image_attributes[0] . '" width="' . $image_attributes[1] . '" height="' . $image_attributes[2] . '">';
				}
			} else {
				$gridClass = 'col-sm-6 col-md-3';
				$itemClass = ' grid-item';
				$dataDom   = 'primopianosmall';
				if ( has_post_thumbnail() ) {
					$imgId   = get_post_thumbnail_id( $post->ID );
					$imgFull = wp_get_attachment_image_src( $imgId, 'full' );
					$imgEcho = '<a href="' . get_permalink() . '"><picture>'
					           . '<!--[if IE 9]><video style="display: none;"><![endif]-->'
					           . '<source media="(min-width: 992px)" data-srcset="' . $imgFull[0] . '?height=125&width=262&enhance=true">'
					           . '<source media="(min-width: 768px)" data-srcset="' . $imgFull[0] . '?height=200&width=768&enhance=true">'
					           . '<source media="(min-width: 414px)" data-srcset="' . $imgFull[0] . '?height=125&width=768&enhance=true">'
					           . '<!--[if IE 9]></video><![endif]-->'
					           . '<img src="' . $imgFull[0] . '?height=177&width=345&enhance=true" alt="' . trim( strip_tags( get_post_meta( $imgId, '_wp_attachment_image_alt', true ) ) ) . '" class="lazyload img-responsive">'
					           . '</picture></a>';

				} else {
					$image_attributes = wp_get_attachment_image_src( 1802, 'sticked' );
					$imgEcho          = '<img src="' . $image_attributes[0] . '" width="' . $image_attributes[1] . '" height="' . $image_attributes[2] . '">';
				}
			}
			$permalink = urlencode( get_permalink() );
			?>
			<!-- .grid-sizer empty element, only used for element sizing -->
			<div class="<?php echo $gridClass; ?>">
				<div class="<?php echo $itemClass; ?>">
					<?php echo $imgEcho; ?>
					<div class="grid-paragraph" data-card="<?php echo $dataDom; ?>">
						<h2 data-updated="<?php if ( CFS()->get( 'updated' ) ) {
							the_modified_time( 'U' );
						} ?>" data-published="<?php the_time( 'U' ); ?>"><a href="<?php the_permalink(); ?>"
						                                                    class="thumb-container"
						                                                    title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a>
						</h2>
						<?php the_excerpt(); ?></div>
					<hr class="styled"/>
					<div class="post-share post-share-home">
                <span>

	                <a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo $permalink; ?>"
                         title="Condividi su Facebook" class="share-link wn-pop"><i
			                class="fa fa-facebook fa-lg icon-facebook" aria-hidden="true"></i><span class="sr-only">Condividi su Facebook</span></a>
                    <a href="https://twitter.com/share?url=<?php echo $permalink; ?>&via=acerbosocial&text=<?php echo urlencode( get_the_title() ); ?>"
                       title="Condividi su Twitter" class="share-link wn-pop"><i class="fa fa-twitter fa-lg icon-twitter" aria-hidden="true"></i><span class="sr-only">Condividi su Twitter</span></a>

	                <a href="<?php echo get_the_permalink(); ?>" data-title="<?php echo urlencode( get_the_title() ); ?>" data-shorturl="<?php echo acerbo_get_short($post->ID); ?>"
	                   title="Condividi su Whatsapp" class="whatsapp-share share-link visible-xs-inline-block"><i
			                class="fa fa-whatsapp fa-lg icon-whatsapp" aria-hidden="true"></i><span class="sr-only">Condividi su Whatsapp</span></a>
                    <a href="https://telegram.me/share/url?url=<?php echo $permalink; ?>&text=<?php echo urlencode( get_the_title() ); ?>"
                       title="Condividi su Telegram" class="share-link visible-xs-inline-block"><i
                                class="fa fa-telegram fa-lg icon-telegram" aria-hidden="true"></i><span class="sr-only">Condividi su Telegram</span></a>
                </span>
					</div>
				</div>
			</div>
			<?php
		endforeach;
		wp_reset_postdata();
		?>
	</div>
</div>
