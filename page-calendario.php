<?php
/*
  Template Name: Template per il calendario
 */

get_header();
?>



    <div id="content" class="col-md-12">

        <div id="page-<?php the_ID(); ?>" <?php post_class('acerbo-entry'); ?>>

            <div class="page-header">
                <h1 class="title compensate-bs">
                    <?php the_title(); ?>
                </h1>
            </div>


            <div class="blocchetto">
                <div class="page-header">
                    <div class="pull-right form-inline">
                        <div class="btn-group"><button class="btn btn-primary" data-calendar-nav="prev"><i class="fa fa-chevron-left"></i> Precedente</button><button class="btn btn-default" data-calendar-nav="today">Oggi</button><button class="btn btn-primary" data-calendar-nav="next">Successivo <i class="fa fa-chevron-right"></i></button></div>
                    </div>
                    <h3></h3>

                </div>
                <div id="calendar"></div>

            </div>

            <?php comments_template('', true); ?>

        </div>
    </div>



<?php get_footer(); ?>